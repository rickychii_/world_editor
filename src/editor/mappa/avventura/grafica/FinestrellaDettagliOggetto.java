package editor.mappa.avventura.grafica;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

//LIMITE: 15 caratteri per id
//LIMITE: 5 caratteri per zindex
public class FinestrellaDettagliOggetto extends JDialog
{
    static final long serialVersionUID = 10;

    protected JTextField textfieldID; 
    protected JTextField textfieldZI;
    protected JTextField  textfieldIMG;
    protected JComboBox<String> jcomboAnimated;
    
    //GESTISCO LE ANIMAZIONI SU UNA FINESTRA A PARTE
    //SPRITEWIDTH = CELLEX
    protected JTextField textfieldSpriteCellsX;
    //SPRITEHEIGHT = CELLEY
    protected JTextField textfieldSpriteCellsY;
    //Y PARTENZA ANIMAZIONE
    //protected JTextField textfieldyAnim;
    //NUMERO RIGHE ANIMAZIONE
    //protected JTextField textfieldspriteLenght;
    
        
    protected JTextField tfPOSX;
    protected JTextField tfPOSY;
    
    protected JTextField _width;
    protected JTextField _height;
    
    protected JTextField _widthSprite;
    protected JTextField _heightSprite;
    
    protected JComboBox<String> jcombocollisioni;
    
    protected Oggetto stolavorandosu;
    
    public FinestrellaDettagliOggetto(Oggetto p, final JFrame sender)
    {
        this.stolavorandosu = p;
        this.setTitle("Dettagli Oggetto " + p.id);
        this.setSize(350, 400);
        this.setResizable(false);
  
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
       
        this.requestFocus();
        
        //CENTRO DELLO SCHERMO!
        this.setLocationRelativeTo(null);
        
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent we) {
                sender.setEnabled(false);
            }

            @Override
            public void windowClosing(WindowEvent we) {
            }

            @Override
            public void windowClosed(WindowEvent we) {
                sender.setEnabled(true);
                sender.requestFocus();
            }

            @Override
            public void windowIconified(WindowEvent we) {
            }

            @Override
            public void windowDeiconified(WindowEvent we) {
            }

            @Override
            public void windowActivated(WindowEvent we) {
            }

            @Override
            public void windowDeactivated(WindowEvent we) {
            }
        });

        
        this.setLayout(new GridLayout(14, 2));
        
        JLabel idOggetto = new JLabel("ID Oggetto");
        textfieldID = new JTextField();
        textfieldID.setDocument(new JTextFieldLimit(15));
        textfieldID.setText(p.id);
        
        JLabel zindex = new JLabel("Z-Index");
        textfieldZI = new JTextField();
        textfieldZI.setDocument(new JTextFieldLimit(5));
        textfieldZI.setText(String.valueOf(p.zindex));
        
        JLabel img = new JLabel("Immagine");
        textfieldIMG = new JTextField();
        textfieldIMG.setEditable(false);
        textfieldIMG.setText(String.valueOf(p.getImmagine().path));
        
        JLabel POSX = new JLabel("Pos X");
        JLabel POSY = new JLabel("Pos Y");
        
        
        tfPOSX = new JTextField();
        tfPOSX.setText(String.valueOf(p.getImmagine().x));
        tfPOSX.setEditable(false);
        
        tfPOSY = new JTextField();
        tfPOSY.setText(String.valueOf(p.getImmagine().y));
        tfPOSY.setEditable(false);
        
        
        JLabel LabelWidth = new JLabel("WIDTH");
        JLabel LabelHeight = new JLabel("HEIGHT");
        
         _width= new JTextField();
        _width.setText(String.valueOf(p.getImmagine().width));
        _width.setEditable(false);
        
        _height = new JTextField();
        _height.setText(String.valueOf(p.getImmagine().height));
        _height.setEditable(false);
        
        
        JLabel LabelWidthSprite = new JLabel("Width Sprite");
        JLabel LabelHeightSprite = new JLabel("Height Sprite");
        
        JTextField _widthSprite = new JTextField();
        JTextField _heightSprite = new JTextField();
        _widthSprite.setEnabled(false);
        _heightSprite.setEnabled(false);
        
        
        
        JLabel isAnimated = new JLabel("Animated");
        jcomboAnimated = new JComboBox<String>();
        jcomboAnimated.addItem("true");
        jcomboAnimated.addItem("false");
        
        if(p.getImmagine().isAnimated)
        {
            jcomboAnimated.setSelectedIndex(0);
            _widthSprite.setText(String.valueOf(p.getImmagine().width / Integer.parseInt(p.getImmagine().getSpriteCellsXY()[0].toString())));
            _heightSprite.setText(String.valueOf(p.getImmagine().height / Integer.parseInt(p.getImmagine().getSpriteCellsXY()[1].toString())));
        }
        else
        {
            jcomboAnimated.setSelectedIndex(1);
            _widthSprite.setText("None");
            _heightSprite.setText("None");
        }
        
        JLabel labelSpriteWidth = new JLabel("Quante Celle X?");
        JLabel labelSpriteHeight = new JLabel("Quante Celle Y?");
       
        textfieldSpriteCellsX = new JTextField();
        textfieldSpriteCellsY = new JTextField();
        textfieldSpriteCellsX.setDocument(new JTextFieldLimit(4));
        textfieldSpriteCellsY.setDocument(new JTextFieldLimit(4));     
        
        textfieldSpriteCellsX.setText(stolavorandosu.getImmagine().getSpriteCellsXY()[0].toString());
        textfieldSpriteCellsY.setText(stolavorandosu.getImmagine().getSpriteCellsXY()[1].toString());        
        
        
        //JLabel spriteLenght = new JLabel("Quante Righe Animazione?");
        //JLabel yAnim = new JLabel("Riga Partenza Animazione");
        
        //textfieldspriteLenght = new JTextField();
        //textfieldspriteLenght.setText(String.valueOf(p.getImmagine().SpriteLenght));
        //textfieldspriteLenght.setDocument(new JTextFieldLimit(5));
        
        //textfieldyAnim = new JTextField();
        //textfieldyAnim.setText(String.valueOf(p.getImmagine().SpriteLenght));
        //textfieldyAnim.setDocument(new JTextFieldLimit(5));
        
        
        
        JButton ok = new JButton("OK");
        JButton annulla = new JButton("Annulla");
        
        JLabel tipocoll = new JLabel("Tipo Collisione");
        
        jcombocollisioni = new JComboBox<String>();
        jcombocollisioni.addItem("not setted");
        jcombocollisioni.addItem("A - Collisione a 3 punti");
        jcombocollisioni.addItem("B - Collisione a 4 punti");
        jcombocollisioni.addItem("C - Collisione a 5 punti");
        jcombocollisioni.addItem("D - Collisione a 6 punti");
        jcombocollisioni.addItem("E - Collisione a 7 punti");
        jcombocollisioni.addItem("F - Collisione a 8 punti");
        
        //IMPOSTO LA COLLISIONE ATTUALE
        for(byte i=0; i<jcombocollisioni.getItemCount(); ++i)
        {
            if(p.getCollisione().equals((jcombocollisioni.getItemAt(i)).substring(0,1)))
            {
                jcombocollisioni.setSelectedIndex(i);
                break;
            }
        }
        
        this.add(idOggetto);
        this.add(textfieldID);
        
        this.add(zindex);
        this.add(textfieldZI);
        
        this.add(img);
        this.add(textfieldIMG);
        
        this.add(POSX);
        this.add(POSY);
        
        this.add(tfPOSX);
        this.add(tfPOSY);
        
        this.add(LabelWidth);
        this.add(LabelHeight);
        
        this.add(_width);
        this.add(_height);      
        
        this.add(LabelWidthSprite);
        this.add(LabelHeightSprite);
        
        this.add(_widthSprite);
        this.add(_heightSprite);
        
        this.add(isAnimated);
        this.add(jcomboAnimated);
        
        this.add(labelSpriteWidth);
        this.add(labelSpriteHeight);
        
        this.add(textfieldSpriteCellsX);
        this.add(textfieldSpriteCellsY);
        
        //this.add(spriteLenght);
        //this.add(yAnim);
        
        //this.add(textfieldspriteLenght);
        //this.add(textfieldyAnim);
        
        this.add(tipocoll);
        this.add(jcombocollisioni);
        
        annulla.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)(ae.getSource());
                JDialog jd = (JDialog)(jb.getParent().getParent().getParent().getParent());
                jd.dispose();
            }
        });
        
        ok.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)(ae.getSource());
                FinestrellaDettagliOggetto fin = (FinestrellaDettagliOggetto)(jb.getParent().getParent().getParent().getParent());
                
                try
                {
                    //METTERE I THROW PRIMA DELL'INIZIALIZZAZIONE DELLA RIGA
                    //int indice = MainPanel.mappa.getIndex(fin.stolavorandosu.id);
                    
                    //PRENDO ANIMATED
                    boolean animated = false;
                    if(fin.jcomboAnimated.getSelectedIndex() == 0)
                        animated=true;
                    
                    //PROVO A COSTRUIRE SOTTOIMMAGINE NEL CASO SIA STATA SELEZIONATA L'ANIMAZIONE
                    //INOLTRE SE CI SONO DATI ALTERATI SPARO UN'ECCEZIONE.
                   
                    //GESTISCO LE ANIMAZIONI SU UNA FINESTRA DEDICATA.
                    if(animated)
                    {
                        //CONTROLLO SE IMPOSTATO A TRUE MA IMMAGINE è NULL
                        if(animated && stolavorandosu.getImmagine().img == null)
                            throw new Exception("Impossibile settare lo stato \"Animated\" su un oggetto senza immagine");
                        
                        
                        int spriteWidth = Integer.parseInt(fin.textfieldSpriteCellsX.getText());
                        int spriteHeight = Integer.parseInt(fin.textfieldSpriteCellsY.getText());
                        
                        if(spriteWidth == 0 || spriteHeight == 0)
                            throw new Exception("nei dati");
                        
                        //GESTISCO EVENTUALI DISCORDANZE CON ANIMAZIONE SELEZIONATA
                        AnimazioneOggetto anim = fin.stolavorandosu.getImmagine().getAnimazioneSelezionata();
                        if(null != anim)
                        {
                            int yAnim = anim.yAnimazione;
                            int righe = anim.righeAnimazione;

                            if(yAnim + righe > spriteHeight)
                                throw new Exception("nei dati");
                        }
                    }
                        
                      /*  
                        25.06.2014: CODICE VECCHIO QUANDO ERA GESTITA UNA SOLO ANIMAZIONE DA QUA.
                        //PROVO A COSTRUIRE SUBIMAGE. SE DA ERRORE VUOL DIRE CHE C'è QUALCOSA CHE NON VA BENE.
                        int xsubimage = 0;
                        int ysubimage = 0;
                        int widthTemp = Integer.parseInt(_width.getText()) / Integer.parseInt(textfieldSpriteWidth.getText());
                        int heightTemp = Integer.parseInt(_height.getText()) / Integer.parseInt(textfieldSpriteHeight.getText());
                        
                        //int ysubimage = Integer.parseInt(textfieldyAnim.getText()) * Integer.parseInt(textfieldSpriteHeight.getText());
                        //int widthTemp = Integer.parseInt(_width.getText()) / Integer.parseInt(textfieldSpriteWidth.getText());
                        //int heightTemp = Integer.parseInt(textfieldSpriteHeight.getText());

                        BufferedImage a = stolavorandosu.getImmagine().img.getSubimage(xsubimage, ysubimage, widthTemp, heightTemp);
                        
                        int spriteWidth = Integer.parseInt(fin.textfieldSpriteWidth.getText());
                        int spriteHeight = Integer.parseInt(fin.textfieldSpriteHeight.getText());
                        int spriteLenght = Integer.parseInt(fin.textfieldspriteLenght.getText());
                        int ySprite = Integer.parseInt(fin.textfieldyAnim.getText());
                        
                        if(spriteWidth == 0 || spriteHeight == 0)
                            throw new Exception("Errore nei dati");
                        
                        if(ySprite + (spriteLenght - 1) >= spriteHeight)
                            throw new Exception("Errore nei dati");
                        
                        if(spriteLenght ==0)
                            throw new Exception("Errore nei dati");
                    }
                    */
                    
                    
                    
                    
                    //COSTRUISCO GLI ELEMENTI CHE MI SERVONO
                    int indice = MainPanel.mappa.getIndex(fin.stolavorandosu.id);
                    
                    //OGGETTO CHE HO IN PANCIA
                    Oggetto p = MainPanel.mappa.popOggetto(indice);

                    //PRENDO IL NUOVO ID E GESTISCO ANCHE SE E' STATO MODIFICATO O MENO
                    String id = fin.textfieldID.getText();
                    
                    int zindex = Integer.parseInt(fin.textfieldZI.getText());
                    String tipoCollisione = ((String)(fin.jcombocollisioni.getSelectedItem())).substring(0, 1);
                    //System.out.println(id+"  "+zindex+"  "+tipoCollisione);

                    

                    
                    
                    //VERIFICO COLLISIONE ATTUALE
                    String oldColl = MainPanel.mappa.getOggetto(p.id).getCollisione();
                    
                    if(!oldColl.equals(tipoCollisione))
                    {
                        //PROVO A SETTARE L'OGGETTO CON NUOVA COLLISIONE
                        Exception ex = MainPanel.mappa.setOggetto(indice,id,zindex,tipoCollisione);
                        if(ex != null)
                            throw new Exception (ex);
                    }
                    else
                    {
                        //SETTO L'OGGETTO SENZA COLLISIONE
                        Exception ex = MainPanel.mappa.setOggetto(indice,id,zindex);
                        if(ex != null)
                            throw new Exception (ex);
                    }
                    

                    
                    //SETTO ANIMATED
                    MainPanel.mappa.listaOggetti.get(indice).getImmagine().isAnimated = animated;
                    
                    //SE ANIMATO PRENDO MISURE PER LE ANIMAZIONI
                    //YSPRITE, SPRITELENGHT

                    //GESTISCO LE ANIMAZIONI SU UNA FINESTRA DEDICATA
                    if(animated==true)
                    {
                        
                        //PRENDO SPRITELENGHT
                        //int spriteLenght = Integer.parseInt(fin.textfieldspriteLenght.getText());
                    
                        //PRENDO YSPRITE
                        //int ySprite = Integer.parseInt(fin.textfieldyAnim.getText());
                        
                        int spriteWidth = Integer.parseInt(fin.textfieldSpriteCellsX.getText());
                        int spriteHeight = Integer.parseInt(fin.textfieldSpriteCellsY.getText());
                        
                        
                        //MainPanel.mappa.listaOggetti.get(indice).getImmagine().ySprite = ySprite;
                        //MainPanel.mappa.listaOggetti.get(indice).getImmagine().SpriteLenght = spriteLenght;
                        MainPanel.mappa.listaOggetti.get(indice).getImmagine().setSpriteCellsXY(spriteWidth, spriteHeight);
                        
                        //SE NON HA ANIMAZIONI INIZIALIZZO IL VETTORE ANIMAZIONI
                        if(MainPanel.mappa.listaOggetti.get(indice).getImmagine().listaAnimazioni == null)
                            MainPanel.mappa.listaOggetti.get(indice).getImmagine().resettaAnimazioni();
                    }
                    
                    //IMPOSTO LISTA ANIMAZIONI A NULL SE NON LO è GIA.
                    else
                    {
                        if(MainPanel.mappa.listaOggetti.get(indice).getImmagine().listaAnimazioni != null)
                            MainPanel.mappa.listaOggetti.get(indice).getImmagine().AzzeraAnimazioni();
                    }
                    
                    //SE COLLISIONE APPENA SETTATA VADO A SETTARE TUTTI I PUNTI (COMPONGO FIGURA BASE)
                    //MA SOLO SE NON è NULL 
                    //E IMMAGINE NON è NULL
                    String newColl = MainPanel.mappa.getOggetto(id).getCollisione();
                    if(!(newColl.toLowerCase().equals("n")))
                    {
                        if(!oldColl.equals(newColl))
                        {
                            if(!(MainPanel.mappa.getOggetto(id).getImmagine().path.toLowerCase().equals("null")))
                            {
                                MainPanel.mappa.getOggetto(id).inizializzaFigura();
                            }
                        }
                    }
                    
                    //DATABIND CON ID NUOVO
                    MainPanel.databindJcombo(id);
                    
                    fin.dispose();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, "Errore "+ex.getMessage(),"Ric.C", JOptionPane.OK_OPTION);
                }
            }
        });
        
        //IMPOSTO TEXTFIELD EDITABILI NEL COSTRUTTORE
        if(jcomboAnimated.getSelectedIndex()==0)
        {
            //this.textfieldspriteLenght.setEditable(true);
            //this.textfieldspriteLenght.setText(String.valueOf(p.getImmagine().SpriteLenght));
            //this.textfieldyAnim.setEditable(true);
            //this.textfieldyAnim.setText(String.valueOf(p.getImmagine().ySprite));
            this.textfieldSpriteCellsX.setEnabled(true);
            this.textfieldSpriteCellsY.setEnabled(true);
        }
        else if(jcomboAnimated.getSelectedIndex()==1)
        {
            //this.textfieldspriteLenght.setEditable(false);
            //this.textfieldspriteLenght.setText(String.valueOf(p.getImmagine().SpriteLenght));
            //this.textfieldyAnim.setEditable(false);
            //this.textfieldyAnim.setText(String.valueOf(p.getImmagine().ySprite));
            this.textfieldSpriteCellsX.setEnabled(false);
            this.textfieldSpriteCellsY.setEnabled(false);
        }
        
        
        //IMPOSTO TEXTFIELD EDITABILI NELL'EVENTO DEL JCOMBOBOX
        jcomboAnimated.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JComboBox jcb =(JComboBox)(ae.getSource());
                FinestrellaDettagliOggetto jd = (FinestrellaDettagliOggetto)(jcb.getParent().getParent().getParent().getParent());
                if(jcb.getSelectedIndex()==0)
                {
                    //jd.textfieldspriteLenght.setEditable(true);
                    //jd.textfieldspriteLenght.setText(String.valueOf(jd.stolavorandosu.getImmagine().SpriteLenght));
                    //jd.textfieldyAnim.setEditable(true);
                    //jd.textfieldyAnim.setText(String.valueOf(jd.stolavorandosu.getImmagine().ySprite));
                    jd.textfieldSpriteCellsX.setEnabled(true);
                    jd.textfieldSpriteCellsY.setEnabled(true);
                }
                else if(jcb.getSelectedIndex()==1)
                {
                    //jd.textfieldspriteLenght.setEditable(false);
                    //jd.textfieldspriteLenght.setText(String.valueOf(jd.stolavorandosu.getImmagine().SpriteLenght));
                    //jd.textfieldyAnim.setEditable(false);
                    //jd.textfieldyAnim.setText(String.valueOf(jd.stolavorandosu.getImmagine().ySprite));
                    jd.textfieldSpriteCellsX.setEnabled(false);
                    jd.textfieldSpriteCellsY.setEnabled(false);
                }
            }
            });
        //DISABILITO COLLISIONI SE NON HO IMMAGINE.
        if(p.getImmagine().path == null)
        {
            jcombocollisioni.setEnabled(false);
        }
        
        
        
        this.add(ok);
        this.add(annulla);
        
        
        this.validate();
        this.setVisible(true);
    }
}