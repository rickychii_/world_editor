package editor.mappa.avventura.grafica;
import editor.mappa.avventura.ric_resources.Risorse;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;
import sun.awt.image.ToolkitImage;

public class EditorMappaAvventuraGrafica {
    
    public final static int width=640;
    public final static int height=480;
    
    public final static String percorsoImmagini = "~/img/";
    
    public static JFrame MainWindow;
    public static final String mainTitle = "Editor Mondo Ric.C";
    
    public static void main(String[] args) {
        FinestrellaDimensioni fin = new FinestrellaDimensioni();
        fin.setVisible(true);
    }   
    
    
    //DEVE CARICARE LA MAPPA IN BASE A UN FILE .XML
    public static void CaricaMappa(File file)
    {
        try
        {
            String path = file.getPath().replace(file.getName(), "");
            
            FileReader filereader = new FileReader(file);
            BufferedReader read = new BufferedReader(filereader);
            
            //PRIMO: MI SERVE LARGHEZZAMAPPA E STRINGA BACKGROUND
            //POI MI SERVE PRENDERE TUTTE LE IMMAGINI E CARICARLE
            //IN RAM.
            
            MainPanel mainPanel = null;
            
            //VARIABILI DI MAPPA
            String larghezzaScena = "";
            String backgroudScena = "";
            
            
            //VARIABILI OGGETTO.
            List<Oggetto> listaOggetti;
            listaOggetti=new ArrayList<>();
            
            
            //STRINGA DI LETTURA FILE.
            String line = null;
            
            //STRINGA TEMPORANEA PER TAGLI STRINGA LETTURA.
            String tmp = null;
            //INT TEMPORANEO PER TAGLI STRINGA LETTURA
            int posCheMiServe=-1;
            
            while((line = read.readLine()) != null)
            {
                //TUTTO MINUSCOLO
                line = line.toLowerCase();
                
                //System.out.println(line);
                //PRIMA RIGA: RIGA DI SCENA PRENDO BACKGROUND E LARGHEZZA
                if(line.contains("<scene id="))
                {
                    //BACKGROUND: ESISTE?
                    //CORREZIONE 08.11.2014!!! (BUG TROVATO AL BAR)
                    if(line.indexOf("~/img") > 0)
                    {
                        tmp = line.substring(line.indexOf("~/img"));
                        posCheMiServe = 0;
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            backgroudScena += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                    }
                    //BACKGROUND NON ESISTE
                    else
                    {
                        backgroudScena="null";
                    }

                    //System.out.println(backgroudScena);
                    
                    //LARGHEZZA
                    tmp = line.substring(line.indexOf("width=") + 7);
                    posCheMiServe=0;
                    while(tmp.charAt(posCheMiServe) != '"')
                    {
                        larghezzaScena += tmp.charAt(posCheMiServe);
                        ++posCheMiServe;
                    }
                    //System.out.println(larghezzaScena);
                    
                    //INTESTAZIONE MAINPANEL
                    mainPanel = new MainPanel(1024, 768, Integer.parseInt(larghezzaScena));
                    
                    
                    MainPanel.mappa = new MappaAttuale();
                    
                    //CORREZIONE 08.11.2014!!! (BUG TROVATO AL BAR)
                    //SE ESISTE L'IMMAGINE DI SFONDO VADO AD IMPOSTARLA:
                    //18.12.2014: STRING.CONTAINS AL POSTO DI INDEXOF
                    if(backgroudScena.contains("~/"))
                    {
                        MainPanel.mappa.pathsfondo = backgroudScena;
                        BufferedImage img = ImageIO.read(new File(path + backgroudScena.replace("~/","")));
                        MainPanel.mappa.sfondo = img.getScaledInstance(Integer.parseInt(larghezzaScena) ,height, BufferedImage.TYPE_INT_RGB);
                    }
                }
                //TROVO INTESTAZIONE DI UN OGGETTO
                else if(line.contains("<object "))
                {
                    //INIZIALIZZO COMPONENTI OGGETTO
                    String idOggetto = "";
                    String zIndexOggetto = "";
                    
                    Oggetto oggTmp = null;
                    
                    posCheMiServe = 0;
                    tmp = line.substring(line.indexOf("id=\"") + 4);
                    while(tmp.charAt(posCheMiServe) != '"')
                    {
                        idOggetto += tmp.charAt(posCheMiServe);
                        ++posCheMiServe;
                    }
                    
                    posCheMiServe = 0;
                    tmp = line.substring(line.indexOf("z-index=\"") + 9);
                    while(tmp.charAt(posCheMiServe) != '"')
                    {
                        zIndexOggetto += tmp.charAt(posCheMiServe);
                        ++posCheMiServe;
                    }
                    
                    //System.out.println(idOggetto + "  " + zIndexOggetto);
                    
                    oggTmp = new Oggetto(idOggetto, Integer.parseInt(zIndexOggetto));
                    ImmagineOggetto oggettoIMG = null;
                    
                    
                    //CERCO SE C'é IMMAGINE
                    line = read.readLine();
                    if(line.contains("<img"))
                    {
                        //IMMAGINE OGGETTO
                        
                        String xIMG = "";
                        String yIMG = "";
                        String widthIMG = "";
                        String heightIMG = "";
                        String isAnimatedIMG = "";
                        String FileIMG = "";

                        //LEGGO 1 ALTRA RIGA: SONO SICURO SIA LA RIGA IMG
                        //PRENDO POSX
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("x=\"") + 3);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            xIMG += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                        
                        //PRENDO POSY
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("y=\"") + 3);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            yIMG += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                        
                        //PRENDO WIDTH
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("width=\"") + 7);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            widthIMG += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                        
                        //PRENDO HEIGHT
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("height=\"") + 8);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            heightIMG += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }

                        //PRENDO ISANIMATED
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("isanimated=\"") + 12);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            isAnimatedIMG += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                        
                        //PRENDO FILENAME
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("filename=\"") + 10);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            FileIMG += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                        
                        FileIMG = FileIMG.replace("~/img/", "");
                        String popo = path + "img\\" + FileIMG;
                        BufferedImage img = ImageIO.read(new File(popo));

                        
                        //System.out.println(xIMG+"  "+yIMG+"  "+widthIMG+"  "+heightIMG+"  "+isAnimatedIMG+"  "+FileIMG);
                        
                        oggettoIMG = new ImmagineOggetto(img, FileIMG);
                        oggettoIMG.x = Integer.parseInt(xIMG);
                        oggettoIMG.y = Integer.parseInt(yIMG);
                        oggettoIMG.width = Integer.parseInt(widthIMG);
                        oggettoIMG.height = Integer.parseInt(heightIMG);
                        oggettoIMG.isAnimated = Boolean.parseBoolean(isAnimatedIMG);
                        
                        oggTmp.setImmagine(oggettoIMG);
                    
                        //LEGGO 1 ALTRA RIGA: VERIFICO SIA RIGA COLLISIONE
                        line = read.readLine();
                    }
                    
                    if(line.contains(("<collision")))
                    {
                        //PRENDO TIPO COLLISIONE:
                        String tipoCollisione = "";
                        
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("type=\"") + 6);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            tipoCollisione += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                        
                        oggTmp.setCollisione(tipoCollisione);
                        
                        //OGGETTO COLLISIONE
                        BaseCollisione oggColl = (BaseCollisione)(oggTmp.getObjectCollisione());
                        
                        //CONTATORE PUNTI: DA 0 A TOT PUNTI
                        byte contatorePunti = 0;
                        
                        String xColl = "";
                        String yColl = "";
                        
                        //CICLO SU TUTTI I PUNTI DI COLLISIONE:
                        while(!(line = read.readLine()).contains("</collision>"))
                        {
                            //STRINGA MODELLO:
                            //<point x="230" y="215" />
                            xColl = "";
                            yColl = "";
                            
                            //PRENDO X
                            posCheMiServe=0;
                            tmp = line.substring(line.indexOf("x=\"") + 3);
                            while(tmp.charAt(posCheMiServe) != '"')
                            {
                                xColl += tmp.charAt(posCheMiServe);
                                ++posCheMiServe;
                            }
                            
                            //PRENDO X
                            posCheMiServe=0;
                            tmp = line.substring(line.indexOf("y=\"") + 3);
                            while(tmp.charAt(posCheMiServe) != '"')
                            {
                                yColl += tmp.charAt(posCheMiServe);
                                ++posCheMiServe;
                            }
                            
                            oggColl.setPunto(contatorePunti, Integer.parseInt(xColl), Integer.parseInt(yColl), oggettoIMG.x, oggettoIMG.y);
                            ++contatorePunti;
                        }
                        
                        
                        //LEGGO 1 ALTRA RIGA: VERIFICO SIA LA RIGA ANIMAZIONE
                        line = read.readLine();
                    }
                    
                    
                    if(line.contains(("<animation")) && oggTmp.getImmagine().isAnimated)
                    {
                        //LEGGO INTESTAZIONE ANIMAZIONE: 
                        //STRINGA MODELLO:
                        //<animation cellsx="2" cellsy="4">
                        
                        String cellX = "";
                        String cellY = "";
                        
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("cellsx=\"") + 8);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            cellX += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                        
                        posCheMiServe=0;
                        tmp = line.substring(line.indexOf("cellsy=\"") + 8);
                        while(tmp.charAt(posCheMiServe) != '"')
                        {
                            cellY += tmp.charAt(posCheMiServe);
                            ++posCheMiServe;
                        }
                        
                        //IMPOSTO CELLEX CELLE Y
                        oggTmp.getImmagine().setSpriteCellsXY(Integer.parseInt(cellX), Integer.parseInt(cellY));
                        
                        //CREO ISTANZA DELLA LISTA ANIMAZIONI
                        oggettoIMG.listaAnimazioni =  new ArrayList<>();
                        
                        String idAnim = "";
                        String ySprite = "";
                        String righeAnim = "";
                        
                        //NUMERO CICLI MI SERVE PER SELEZIONARE LA PRIMA ANIMAZIONE
                        int numeroCicli = 0;
                        //CICLO SU TUTTE LE ANIMAZIONI
                        while(!(line = read.readLine()).contains("</animation>"))
                        {
                            //STRINGA MODELLO:
                            //<motion id="camminata" ysprite="0" righe="4" />
                            
                            idAnim = "";
                            ySprite = "";
                            righeAnim = "";
                            
                            posCheMiServe=0;
                            tmp = line.substring(line.indexOf("id=\"") + 4);
                            while(tmp.charAt(posCheMiServe) != '"')
                            {
                                idAnim += tmp.charAt(posCheMiServe);
                                ++posCheMiServe;
                            }
                            
                            posCheMiServe=0;
                            tmp = line.substring(line.indexOf("ysprite=\"") + 9);
                            while(tmp.charAt(posCheMiServe) != '"')
                            {
                                ySprite += tmp.charAt(posCheMiServe);
                                ++posCheMiServe;
                            }
                            
                            posCheMiServe=0;
                            tmp = line.substring(line.indexOf("righe=\"") + 7);
                            while(tmp.charAt(posCheMiServe) != '"')
                            {
                                righeAnim += tmp.charAt(posCheMiServe);
                                ++posCheMiServe;
                            }
                            
                            AnimazioneOggetto anim = new AnimazioneOggetto(idAnim, Integer.parseInt(ySprite), Integer.parseInt(righeAnim));
                            
                            if(numeroCicli == 0)
                                anim.eSelezionata = true;
                            
                            boolean add = oggettoIMG.listaAnimazioni.add(anim);
                            
                            ++numeroCicli;
                        }
                    } 
                    listaOggetti.add(oggTmp);
                    
                    while(! line.contains("</object>"))
                        line = read.readLine();
                    
                }
            }
            
            //AGGIUNGO LISTA OGGETTI
            MainPanel.mappa.listaOggetti = null;
            MainPanel.mappa.listaOggetti = listaOggetti;
            
            
            //CODICE DA ESEGUIRE QUANDO HO LA LARGHEZZA PRONTA.
            //CREO LA FINESTRA E CHIUDO LA FINESTRA ATTUALE
            editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.dispose();
                    
            editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow 
                = new JFrame(EditorMappaAvventuraGrafica.mainTitle);
                    
            //IMPOSTO R PERSONALIZZATA
            Risorse r=new Risorse();
            ImageIcon icon = new ImageIcon(r.iconURL);
                    
            EditorMappaAvventuraGrafica.MainWindow.setIconImage(icon.getImage());
                    
                    
            editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setSize(1024, 768);
            editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setResizable(false);
            editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            //CENTRO DELLO SCHERMO!
            editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setLocationRelativeTo(null);

            //DATABIND SUL JCOMBO
            MainPanel.databindJcombo("");
            
            editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.add(mainPanel);
            editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setVisible(true);
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Errore nel caricamento della mappa " + ex.toString(), "Ric.C", JOptionPane.OK_OPTION);
        }
    }
    //GENERA L'XML E I VARI FILES (IMMAGINI SU SOTTOCARTELLA /IMG E FILE XML NELLA CARTELLA SELEZIONATA.)
    //SE NON ESISTE /IMG QUESTA VERRA' CREATA.
    //02.07.2014 
    public static void SalvaMappa(File fileToSave)
    {            
        try
        {
            String filename = fileToSave.getName().replace(".xml", "");
                                        
            //1. SALVO FILE MAPPA XML
            FileWriter fw = new FileWriter(fileToSave);
            fw.write(MainPanel.mappa.getScript().replaceAll("<scene id=\"\"", "<scene id=\"" + filename + "\""));
            fw.close();

            //2.CREO LA SOTTODIRECTORY - img
            File dir = new File(fileToSave.getPath().replace(filename, "").replace(".xml","") + "/img");
            dir.mkdir();    
            
            //3. SALVO IMMAGINE SFONDO
            //ESISTE OGGETTO?
            if(! (null == MainPanel.mappa.sfondo))
            {
                String nomeFile = MainPanel.mappa.pathsfondo.replace(EditorMappaAvventuraGrafica.percorsoImmagini, "");
                //System.out.println(dir + "/" + nomeFile);
                
                BufferedImage bi = ((ToolkitImage) MainPanel.mappa.sfondo).getBufferedImage();
                File outputfile = new File(dir + "\\" + nomeFile);
                ImageIO.write(bi, nomeFile.substring(nomeFile.lastIndexOf(".") + 1), outputfile);
                
                //System.out.println(dir + "\\" + nomeFile);
                //System.out.println(nomeFile.substring(nomeFile.lastIndexOf(".")));
            }
            
            
            //4. SALVO TUTTE LE IMMAGINI DEGLI OGGETTI
            for(int i=0; i< MainPanel.mappa.getNumeroOggetti(); ++i)
            {
                //IMMAGINE è NULL??
                if(null == MainPanel.mappa.listaOggetti.get(i).getImmagine().img)
                    continue;
                
                BufferedImage bi = MainPanel.mappa.listaOggetti.get(i).getImmagine().img;
                String nomeFile = MainPanel.mappa.listaOggetti.get(i).getImmagine().path.replace(EditorMappaAvventuraGrafica.percorsoImmagini, "");
                File outputfile = new File(dir + "\\" + nomeFile);
                ImageIO.write(bi, nomeFile.substring(nomeFile.lastIndexOf(".") + 1), outputfile);
                
                //System.out.println(dir + "\\" + nomeFile);
                //System.out.println(nomeFile.substring(nomeFile.lastIndexOf(".")));
            }
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Errore nel salvataggio della mappa " + ex.toString(),"Ric.C", JOptionPane.OK_OPTION);
        }
    }
}