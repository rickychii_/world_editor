package editor.mappa.avventura.grafica;

import editor.mappa.avventura.ric_resources.Risorse;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class FinestrellaDimensioni extends JDialog
{
    protected static int widthAtt;
    protected static JTextField testoDimensioni = new JTextField();

    static final long serialVersionUID = 10;
    
    public FinestrellaDimensioni()
    {
        this.setTitle(EditorMappaAvventuraGrafica.mainTitle);
        widthAtt=640;
        this.setResizable(false);
        this.setSize(400,200);
        //CENTRO DELLO SCHERMO
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);     
        testoDimensioni.setDocument(new JTextFieldLimit(4));
        testoDimensioni.setEditable(false);
        Font font = new Font("Verdana", Font.TYPE1_FONT, 20);
        testoDimensioni.setFont(font);
        testoDimensioni.setText(String.valueOf(widthAtt));
        JButton ok = new JButton("OK");
        JButton piu = new JButton("+");
        piu.addMouseListener(new MouseListener(){
            private Boolean mousepressed=false;
            @Override
            public void mousePressed(MouseEvent me) {
                //JButton b = (JButton)(me.getSource());
                //JPanel j = (JPanel)(b.getParent());
                //FinestrellaDimensioni fin = (FinestrellaDimensioni)(j.getParent().getParent().getParent());
                mousepressed = true;
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        while(mousepressed)
                        {
                            if(FinestrellaDimensioni.widthAtt<3000)
                            {
                                FinestrellaDimensioni.widthAtt += 20;
                                FinestrellaDimensioni.testoDimensioni.setText(String.valueOf(FinestrellaDimensioni.widthAtt));
                            }
                            try
                            {
                                Thread.sleep(180);
                            }
                            catch(InterruptedException ex)
                            {
                                System.err.println(ex.toString());
                            }
                        }
                    }
                }.start();
            }

            @Override
            public void mouseReleased(MouseEvent me) {
                mousepressed=false;
            } 
            @Override
            public void mouseEntered(MouseEvent me) {
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }

            @Override
            public void mouseClicked(MouseEvent me) {
            }
        });
        JButton meno = new JButton("-");
        meno.addMouseListener(new MouseListener(){
            private Boolean mousepressed=false;
            @Override
            public void mousePressed(MouseEvent me) {
                //JButton b = (JButton)(me.getSource());
                //JPanel j = (JPanel)(b.getParent());
                //FinestrellaDimensioni fin = (FinestrellaDimensioni)(j.getParent().getParent().getParent());
                mousepressed = true;
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        while(mousepressed)
                        {
                            if(FinestrellaDimensioni.widthAtt>640)
                            {
                                FinestrellaDimensioni.widthAtt -= 20;
                                FinestrellaDimensioni.testoDimensioni.setText(String.valueOf(FinestrellaDimensioni.widthAtt));
                            }
                            try
                            {
                                Thread.sleep(180);
                            }
                            catch(InterruptedException ex)
                            {
                                System.err.println(ex.toString());
                            }
                        }
                    }
                }.start();
            }
            @Override
            public void mouseReleased(MouseEvent me) {
                mousepressed=false;
            } 
            @Override
            public void mouseEntered(MouseEvent me) {
            }
            @Override
            public void mouseExited(MouseEvent me) {
            }
            @Override
            public void mouseClicked(MouseEvent me) {
            }
        });
        ok.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
               try
               {
                   int Percwidth = Integer.parseInt(testoDimensioni.getText());
                   if(Percwidth > 3000 || Percwidth < 640) 
                   {
                       throw new NumberFormatException ("Attenzione! valori errati");
                   }
                    //CODICE DA ESEGUIRE QUANDO HO LA LARGHEZZA PRONTA.
                    //CREO LA FINESTRA E CHIUDO LA FINESTRELLA DELLE DIMENSIONI.
                    editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow 
                            = new JFrame(EditorMappaAvventuraGrafica.mainTitle);
                    
                    //IMPOSTO R PERSONALIZZATA
                    Risorse r = new Risorse();

                    ImageIcon icon = new ImageIcon(r.iconURL);
                    
                    EditorMappaAvventuraGrafica.MainWindow.setIconImage(icon.getImage());
                    
                    MainPanel mainPanel=new MainPanel(1024, 768, Percwidth);
                    
                    editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setSize(1024, 768);
                    editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setResizable(false);
                    editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    //CENTRO DELLO SCHERMO!
                    editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setLocationRelativeTo(null);

                    editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.add(mainPanel);
                    editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.MainWindow.setVisible(true);


                    JButton b = (JButton)(ae.getSource());
                    JPanel j = (JPanel)(b.getParent());
                    ((JDialog)(j.getParent().getParent().getParent())).dispose();
               }
               catch(NumberFormatException  ex)
               {
                    JOptionPane.showMessageDialog(null,"Attenzione! "+ex.toString());
               }
            }
        });
        this.add(new JLabel("Larghezza in px (min 640px max 3000px)"));
        this.add(testoDimensioni);
        this.add(piu);
        this.add(meno);
        this.add(ok);
        this.setLayout(new GridLayout(5,1));
    }
}

class JTextFieldLimit extends PlainDocument {
  private final int limit;
  JTextFieldLimit(int limit) {
    super();
    this.limit = limit;
  }

  JTextFieldLimit(int limit, boolean upper) {
    super();
    this.limit = limit;
  }

  @Override
  public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
    if (str == null)
      return;

    if ((getLength() + str.length()) <= limit) {
      super.insertString(offset, str, attr);
    }
  }
}