package editor.mappa.avventura.grafica;

import editor.mappa.avventura.ric_resources.Risorse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.util.*;

public class PannelloImmagine extends JPanel 
    implements MouseMotionListener, MouseListener, KeyListener, Runnable   {

    static final long serialVersionUID = 10;
   
    //COORDINATA X DEL MOUSE (SI AGGIUNGE SHIFT X PER OTTENERE X RELATIVO A MAPPA SHIFTATA)
    private static int x;
    
    //COORDINATA Y DEL MOUSE
    private static int y;
    
    private int currentwidth;
    
    public static int maxshiftX;
    public static int minshiftX;
    public static int shiftX;
     
    private int old_mousex;
    
    private static boolean stoPremendoMouseCentrale;
    static boolean mousedentro;
    boolean snapAttivo;

    //WIDTH HEIGHT SONO DIVERSE 
    //DA MAIN.width e MAIN.height:
    /*
    hanno qualche pixel in piu.
    se devo riferirmi alla grandezza assoluta
    vado a chiamare sempre width e heith main!
    */
    
    private final int width;
    private final int height;
    
    private final int ColoreCLS=0xCECECE;
    
    int xpartenzadisegno;
    int ypartenzadisegno;
    boolean stodisegnando;
    
    //VETTORE DI OGGETTI SELEZIONATI
    private static java.util.List<String> oggettiSelezionati;
    
    BufferedImage cursor;
    
    
    Font font_NumeroCollisione = new Font("Verdana",Font.BOLD,16);
    
    /*
    10.04.2014:
    aggiustato shift, creato snap, messo righello.
    
    11.04.2014
    fixato snap e gestite le dimensioni (width, height)
    
    12.04.2014
    gestire gli oggetti: posso aggiungere oggetti e linee/immagini per quell'oggetto.
    
    27.04.2014
    aggiunto caricamento immagine per oggetto.
    
    18.05.2014
    aggiunta selezione (e spostamento) immagini e aggiunti dettagli oggetto posx, posy, width, height su finestrella dettagli
    
    26.05.2014
    cominciate figure base sulle collisioni (FATTO QUADRATO)
    
    07.06.2014
    fatte tutte le figure e finito spostamento vertici.
    
    08.06.2014
    aggiunti dettagli collisioni (numeri e finestrella dettagli)
    
    14.06.2014
    riposizionamento etichette con numero vertice collisione
    limite grandezza sprites animati.
    
    23.06.2014
    sistemate incongruenze dimensioni sprites
    sistemata selezione sprite con animazione
    sistemato spostamento sprite con animazione
    
    24.06.2014
    iniziate procedure per gestire multianimazioni su singolo oggetto (modificato xml)
    
    27.06.2014
    fatta la finestra per gestire le multianimazioni, ma ho trovato problemi.
    
    28.06.2014
    creato thread per gestire itemselezionato su finestra dettaglio animazioni:
    finiti dettagli multianimazioni. stampato primo frame dell'animazione in caso di animated=true.
    impostata grafica sul menu
    creata impostazione sfondo.
    fatte animazioni sprites :D
    risolto bug selezione su oggetti animati.
    
    29.06.2014
    creata finestra dettagli collisioni.
    risolto piccolo bug nella stampa delle immagini animate durante lo shift della mappa.
    generato script XML della mappa
    
    02.07.2014
    salvataggio file XML
    
    03.07.2014
    salvate immagini su sottocartella /img/
    
    03.08.2014
    caricamento mappa
    */
    
    public PannelloImmagine(int width, int height)
    {
        x=0;
        y=0;
        //TRASLAZIONE ATTUALE
        shiftX=0;
        
        //MAX è SEMPRE 0
        maxshiftX = 0;
        
        //MIN SEMPRE PUNTO MINIMO CALCOLATO CON QUESTA FORMULA SEMPRE NEGATIVO.
        //LO SHIFT VA DA -min a MAX
        //E SCORRE DA SINISTRA A DESTRA
        minshiftX =  (editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.width - width);
        
        stoPremendoMouseCentrale=false;
        snapAttivo=false;
        stodisegnando=false;

        oggettiSelezionati = new  ArrayList<String>();
        
        //width che copre tutta l'area...
        this.width=width;
        this.height=height;
        this.setLayout(null);
        this.setSize(editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.width+2, height+2);
        this.currentwidth=editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.width;
        
        this.addMouseMotionListener(this);
        this.addMouseListener(this);

        this.addKeyListener(this);
        
        this.setFocusable(true);
        this.requestFocus();
        
        this.setCursor(
                getToolkit().createCustomCursor( new BufferedImage( 1, 1, BufferedImage.TYPE_INT_ARGB ), new Point(), null ));
        
        //CARICO IL CURSORE PERSONALIZZATO
        Risorse r=new Risorse();
        try
        {
            cursor=ImageIO.read(r.cursor);
        }
        catch(java.io.IOException ex)
        {
            System.err.println(ex.toString());
        }
        
        new Thread(this).start();
    }
    
    @Override
    public void run()
    {
        while(true)
        {
            try
            {
                Thread.sleep(20);
                this.repaint();
            }
            catch(InterruptedException ex)
            {
                System.err.println(ex.toString());
            }
        }
    }
    
    @Override
    protected void paintComponent(Graphics g) 
    {   
        super.paintComponent(g);
        
        //cls
        g.setColor(new Color(ColoreCLS));
        g.fillRect(0, 0, EditorMappaAvventuraGrafica.width + 80, height+50);
        
        
        //DISEGNO IMMAGINE DI SFONDO
        if(MainPanel.mappa.sfondo != null)
            g.drawImage(MainPanel.mappa.sfondo, 0 - shiftX, 0 , this);
        
        
        //DISEGNO TUTTI GLI OGGETTI DELLA MAPPA!
        //ORDINATI CON Z-INDEX:
        //ULTIMO AGGIORNAMENTO:
        //04.05.2014
        //STAMPA OGGETTI DA ZINDEX BASSO A ZINDEX ALTO
        //STAMPO OGGETTI CON IMMAGINE ANIMATE, SE SERVE: 28.06.2014
        if(MainPanel.getSelectedObject().toLowerCase().equals("all"))
        {   
            int[] indiciconzindex = BubbleSortapplicatoaLista.getIndiciOrderBY(MainPanel.mappa.listaOggetti);
            
            for(int i=0; i<MainPanel.mappa.listaOggetti.size(); ++i)
            {
                Oggetto target = (Oggetto)(MainPanel.mappa.popOggetto(indiciconzindex[i]));
                
                //IMMAGINE OGGETTO
                //SE NON STO ANIMANDO PRENDO IMMAGINE STATICA (EVENTUALMENTE SUBIMAGE)
                //ALTRIMENTI PRENDO IMMAGINE GESTITA DAL THREAD ANIMAZIONE.
                BufferedImage img;
                boolean einMovimento = false;
            
                if(!MainPanel.stoAnimando)
                {
                    img = target.getImmagine().img;
                }
                else
                {
                    if(target.getImmagine().isAnimated)
                    {
                        img = target.getImmagine().getFrame();
                        einMovimento = true;
                    }
                    else
                    {
                        img = target.getImmagine().img;
                    }
                }
                if(img != null)
                {
                    if(!target.getImmagine().isAnimated)
                    {
                        int widthImg = target.getImmagine().width;
                        int heightImg = target.getImmagine().height;

                        int xImg = target.getImmagine().x -shiftX;
                        int yImg = target.getImmagine().y;

                        //Y MAI SOGGETTO A SHIFT
                        if(target.getImmagine().height > height)
                        {
                            heightImg=height;
                        }


                        g.drawImage(img.getSubimage(0, 0, widthImg, heightImg),
                                xImg, 
                                yImg,  this);
                    }
                    //DISEGNO IMMAGINE RITAGLIATA IN BASE ALLE PROPRIETA' DELLO SPRITE
                    else
                    {
                        //SE IN MOVIMENTO STAMPO FRAME.
                        if(einMovimento)
                        {
                            g.drawImage(img, target.getImmagine().x - shiftX, target.getImmagine().y,img.getWidth(),img.getHeight(),this);
                        }
                        else
                        {
                            //PRENDO PROPRIETA' ANIMAZIONE ATTUALMENTE SELEZIONATA
                            AnimazioneOggetto anim = target.getImmagine().getAnimazioneSelezionata();

                            //WIDTH/CELLEX 
                            int widthImg = target.getImmagine().width / (Integer)(target.getImmagine().getSpriteCellsXY()[0]);
                            int heightImg = target.getImmagine().height / (Integer)(target.getImmagine().getSpriteCellsXY()[1]);

                            int xImg = target.getImmagine().x -shiftX;
                            int yImg = target.getImmagine().y;

                            int xsubimage = 0;
                            int ysubimage = anim.yAnimazione * heightImg;

                            g.drawImage(img.getSubimage(xsubimage, ysubimage, widthImg, heightImg),
                                    xImg, 
                                    yImg,  this);
                        }
                    }
                }
                
                //COLLISIONE OGGETTO: DISEGNO LINEE CHE UNISCONO LA FIGURA. E MANIGLIE
                BaseCollisione coll = ((BaseCollisione)(target.getObjectCollisione()));
                
                if(coll != null)
                {
                    //LINEE..
                    Graphics2D g22 = (Graphics2D) g;
                    g22.setStroke(new BasicStroke(3));
                    g22.setColor(Color.BLACK);
                    Point[] p = coll.getPunti();
                    for(int ip=0; ip < p.length -1; ++ip)
                    {
                        g22.drawLine(p[ip].x - shiftX, p[ip].y, p[ip+1].x - shiftX, p[ip+1].y);
                    }
                    g22.drawLine(p[p.length-1].x - shiftX, p[p.length-1].y, p[0].x - shiftX, p[0].y);
                    
                    //RIPORTO SPESSORE A 1
                    g22.setStroke(new BasicStroke(1));

                    //DISEGNO MANIGLIE E MANIGLIA SELEZIONATA....
                    for(int ip=0; ip < p.length; ++ip)
                    {
                        if(coll.getIndexPuntoSelezionato() == ip)
                            g.setColor(Color.RED);
                        else
                            g.setColor(Color.BLUE);
                        g.fillOval(p[ip].x-5 - shiftX, p[ip].y-5, 10, 10);
                        
                        //SCRIVO IL NUMERO IN PARTE IN BASE ALLA POSIZIONE DEL VERTICE
                        int vx = 0;
                        int vy = 0;
                        int scrittax = 0;
                        int scrittay = 0;
                        if(p[ip].x < EditorMappaAvventuraGrafica.width / 2)
                        {
                            //1
                            if(p[ip].y < EditorMappaAvventuraGrafica.height /2)
                            {
                                vx = 15;
                                vy = 15;
                                scrittax = 20;
                                scrittay = 30;
                            }
                            //3 
                            else
                            {
                                vx = 15;
                                vy = -25;
                                scrittax = 20;
                                scrittay = -10;
                            }
                        }
                        else
                        {
                            //2
                            if(p[ip].y < EditorMappaAvventuraGrafica.height /2)
                            {
                                vx = -15;
                                vy = 25;
                                scrittax = -10;
                                scrittay = 40;
                            }
                            //4
                            else
                            {
                                vx = -15;
                                vy = -35;
                                scrittax = -10;
                                scrittay = -20;
                            }
                        }
                        
                        
                        g.setColor(Color.BLACK);
                        g.fillRect(p[ip].x+vx - shiftX, p[ip].y+vy, 20, 20);
                        g.setColor(Color.WHITE);
                        g.setFont(font_NumeroCollisione);
                        g.drawString(String.valueOf(ip), p[ip].x + scrittax - shiftX, p[ip].y + scrittay);
                    }
                }
            }
        }
        else
        {
            String idoggettodadisegnare = MainPanel.getSelectedObject();
            Oggetto target = MainPanel.mappa.getOggetto(idoggettodadisegnare);
            
            //IMMAGINE OGGETTO
            //SE NON STO ANIMANDO PRENDO IMMAGINE STATICA (EVENTUALMENTE SUBIMAGE)
            //ALTRIMENTI PRENDO IMMAGINE GESTITA DAL THREAD ANIMAZIONE.
            BufferedImage img;
            boolean einMovimento = false;
            
            if(!MainPanel.stoAnimando)
            {
                img = target.getImmagine().img;
            }
            else
            {
                if(target.getImmagine().isAnimated)
                {
                    img = target.getImmagine().getFrame();
                    einMovimento = true;
                }
                else
                {
                    img = target.getImmagine().img;
                }
            }
            
            if(img!=null)
            {
                
                if(!target.getImmagine().isAnimated)
                {
                    int widthImg = target.getImmagine().width;
                    int heightImg = target.getImmagine().height;

                    int xImg = target.getImmagine().x -shiftX;
                    int yImg = target.getImmagine().y;

                    //Y MAI SOGGETTO A SHIFT
                    if(target.getImmagine().height > height)
                    {
                        heightImg=height;
                    }
                    g.drawImage(img.getSubimage(0, 0, widthImg, heightImg),
                            xImg, 
                            yImg,  this);
                                        //DISEGNO IMMAGINE RITAGLIATA IN BASE ALLE PROPRIETA' DELLO SPRITE
                }
                else
                {
                    //SE IN MOVIMENTO STAMPO FRAME.
                    if(einMovimento)
                    {
                        g.drawImage(img, target.getImmagine().x - shiftX, target.getImmagine().y,img.getWidth(),img.getHeight(),this);
                    }
                    else
                    {
                        //PRENDO PROPRIETA' ANIMAZIONE ATTUALMENTE SELEZIONATA
                        AnimazioneOggetto anim = target.getImmagine().getAnimazioneSelezionata();

                        //WIDTH/CELLEX 
                        int widthImg = target.getImmagine().width / (Integer)(target.getImmagine().getSpriteCellsXY()[0]);
                        int heightImg = target.getImmagine().height / (Integer)(target.getImmagine().getSpriteCellsXY()[1]);

                        int xImg = target.getImmagine().x -shiftX;
                        int yImg = target.getImmagine().y;

                        int xsubimage = 0;
                        int ysubimage = anim.yAnimazione * heightImg;

                        g.drawImage(img.getSubimage(xsubimage, ysubimage, widthImg, heightImg),
                                xImg, 
                                yImg,  this);
                    }
                }
            }
            //COLLISIONE OGGETTO: DISEGNO LINEE CHE UNISCONO LA FIGURA. E MANIGLIE
            BaseCollisione coll = ((BaseCollisione)(target.getObjectCollisione()));
            if(coll != null)
            {
                //LINEE..
                Graphics2D g22 = (Graphics2D) g;
                g22.setStroke(new BasicStroke(3));
                g22.setColor(Color.BLACK);
                Point[] p = coll.getPunti();
                for(int ip=0; ip < p.length -1; ++ip)
                {
                    g22.drawLine(p[ip].x - shiftX, p[ip].y, p[ip+1].x - shiftX, p[ip+1].y);
                }
                g22.drawLine(p[p.length-1].x - shiftX, p[p.length-1].y, p[0].x - shiftX, p[0].y);

                //RIPORTO SPESSORE A 1
                g22.setStroke(new BasicStroke(1));

                //DISEGNO MANIGLIE E MANIGLIA SELEZIONATA....
                for(int ip=0; ip< p.length; ++ip)
                {
                    if(coll.getIndexPuntoSelezionato() == ip)
                        g.setColor(Color.RED);
                    else
                        g.setColor(Color.BLUE);
                    g.fillOval(p[ip].x-5 - shiftX, p[ip].y-5, 10, 10);


                                            //SCRIVO IL NUMERO IN PARTE IN BASE ALLA POSIZIONE DEL VERTICE
                    int vx = 0;
                    int vy = 0;
                    int scrittax = 0;
                    int scrittay = 0;
                    if(p[ip].x < EditorMappaAvventuraGrafica.width / 2)
                    {
                        //1
                        if(p[ip].y < EditorMappaAvventuraGrafica.height /2)
                        {
                            vx = 15;
                            vy = 15;
                            scrittax = 20;
                            scrittay = 30;
                        }
                        //3 
                        else
                        {
                            vx = 15;
                            vy = -25;
                            scrittax = 20;
                            scrittay = -10;
                        }
                    }
                    else
                    {
                        //2
                        if(p[ip].y < EditorMappaAvventuraGrafica.height /2)
                        {
                            vx = -15;
                            vy = 25;
                            scrittax = -10;
                            scrittay = 40;
                        }
                        //4
                        else
                        {
                            vx = -15;
                            vy = -35;
                            scrittax = -10;
                            scrittay = -20;
                        }
                    }


                    //SCRIVO IL NUMERO IN PARTE
                    g.setColor(Color.BLACK);
                    g.fillRect(p[ip].x + vx - shiftX, p[ip].y + vy, 20, 20);
                    g.setColor(Color.WHITE);
                    g.setFont(font_NumeroCollisione);
                    g.drawString(String.valueOf(ip), p[ip].x + scrittax - shiftX, p[ip].y + scrittay);
                }
            }
        }
        
        //DISEGNO GRIGLIA
        g.setColor(new Color(0xAA3333));
        //RIGHE VERTICALI: 
        //METTO LO SHIFT.
        for(int j=minshiftX; j < EditorMappaAvventuraGrafica.width; j+=20)
        {
            if(j+shiftX>=0 && j+shiftX <= EditorMappaAvventuraGrafica.width)
                g.drawLine(j + shiftX, 0, j + shiftX, height);
        }
        
        //RIGHE ORIZZONTALI:
        //NON SERVE LO SHIFT.
        for(int i=0; i<editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.height ;i+=20)
        {
            g.drawLine(0,i, EditorMappaAvventuraGrafica.width, i);
        }
        
        //DISEGNO QUADRATI PER LO SNAP
        g.setColor(Color.BLUE);
        if(snapAttivo)
        {
            for(int j=0; j<=editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.height; j+=20)
                for( int i=minshiftX; i<= EditorMappaAvventuraGrafica.width; i+=20)
                {
                    if(i+shiftX>=0 && i+shiftX <= EditorMappaAvventuraGrafica.width)
                        //g.drawRect(i-1 + shiftX, j-1 , 2, 2);
                        g.fillOval(i-2 +shiftX, j-2, 4, 4);
                }
        }
        
        //DISEGNO BORDI:
        //20,20,
        //width (640),
        //height (480)
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, 641, 481);
        
        //ULTIMI 2: HANNO ENTRAMBI LINEA PIU SPESSA:
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2));
                    
        //LINEE GUIDA
        if(mousedentro && !stoPremendoMouseCentrale)
        {
            g.setColor((Color.CYAN));

            g2.drawLine(x, y, 0, y);
            g2.drawLine(x,y,x,width);
        }
        
        //EVENTUALI LINEE.....
        //DEPRECATO
        //LINEA CORRENTE
        /*
        if(MainPanel.strumentolinea==true && stodisegnando)
        {
            g.setColor(Color.yellow);
            g.drawRect(x-5, y-5, 10, 10);
            g.drawRect(xpartenzadisegno-5, ypartenzadisegno-5, 10, 10);
            g.setColor(Color.RED);
            g.drawLine(xpartenzadisegno, ypartenzadisegno, x, y);
        } */  
        
        //DISEGNO OGGETTI SELEZIONATI
        for(int i=0; i<oggettiSelezionati.size(); ++i)
        {
            String id = oggettiSelezionati.get(i);
            Oggetto tmp = MainPanel.mappa.getOggetto(id);
            
            ImmagineOggetto img = tmp.getImmagine();
            g.setColor(Color.red);
            if(!img.isAnimated)
                g.drawRect(img.x - shiftX, img.y, img.width,img.height);
            else    
            {
                //WIDTH/CELLEX 
                int widthImg = img.width / (Integer)(img.getSpriteCellsXY()[0]);
                int heightImg = img.height / (Integer)(img.getSpriteCellsXY()[1]);

                int xImg = img.x -shiftX;
                int yImg = img.y;

                g.drawRect(xImg, yImg, widthImg, heightImg);
            }
        }
        
        
        //DISEGNO CURSORE MOUSE
        if(!stoPremendoMouseCentrale && mousedentro == true)
            g.drawImage(cursor, x-cursor.getWidth()/2, y-cursor.getHeight()/2, this);
    }
    //per lo spostamento del pannello immagine
    public static int get_X()
    {
        if(!stoPremendoMouseCentrale)
            return x + shiftX ;
        else
            return 0;
    }
    
    public static int get_Y()
    {
        if(!stoPremendoMouseCentrale)
            return y;
        else
            return 0;
    }

    public static void azzeraOggettiSelezionati()
    {
        oggettiSelezionati.clear();
    }
    
    //FUNZIONE CHE FA LO SNAP
    private void snappa(MouseEvent me)
    {      
        //Correzione del 28.04.2014 riguardante X
        int px = (me.getX() / 20) * 20 + (shiftX % 20);
        if(px < 640 + 5)
            this.x = px; //(me.getX() / 20) * 20 + (shiftX % 20);
         
        this.y = (me.getY() / 20) * 20;
    }
    
    @Override
    public void mouseDragged(MouseEvent me) {
        //GESTIRE DRAG!
        if(stoPremendoMouseCentrale)
        {            
            //MUOVO A DESTRA
            if(old_mousex > x && shiftX <  Math.abs(minshiftX))
            {
                shiftX+=10;
            }
            //MUOVO A SINISTRA
            else if(old_mousex < x && shiftX > maxshiftX)
            {
                shiftX-=10;
            }
            currentwidth=editor.mappa.avventura.grafica.EditorMappaAvventuraGrafica.width + shiftX;
        }
        //MOVIMENTO SENZA E CON DRAG
        if(!this.snapAttivo)
        {
            if(me.getX() + shiftX <= (currentwidth))
                this.x=me.getX();
            if(me.getY() <= this.height)
                this.y=me.getY();
        }
        else
        {
            if(me.getX() + shiftX <= (currentwidth) &&
                (me.getY() <= this.height))
                snappa(me);
        }
        //PREMUTO TASTO SINISTRO:
        if (SwingUtilities.isLeftMouseButton(me))
        {
            //MUOVO VERTICI SELEZIONATI
            for(int i=0; i<MainPanel.mappa.getNumeroOggetti(); ++i)
            {
                Oggetto tmp = MainPanel.mappa.popOggetto(i);
                BaseCollisione coll = (BaseCollisione)(tmp.getObjectCollisione());
                
                
                if(coll!=null)
                {   
                    if(coll.getIndexPuntoSelezionato() > -1)
                    {
                        Point p = coll.getPunti()[coll.getIndexPuntoSelezionato()];
                        coll.spostaPunto(coll.getIndexPuntoSelezionato(),
                                PannelloImmagine.x,
                                PannelloImmagine.y,
                                tmp.getImmagine().y,
                                tmp.getImmagine().x);
                    }
                }
            }
            
            
            
            //MUOVO OGGETTI SELEZIONATI
            for(int i=0;i<oggettiSelezionati.size(); ++i)
            {
                Oggetto tmp = MainPanel.mappa.getOggetto(oggettiSelezionati.get(i));
                ImmagineOggetto img = tmp.getImmagine();
                if(!tmp.getImmagine().isAnimated)
                {
                    if(x >= 0 && y >= 0 &&
                       x + shiftX + img.width <= EditorMappaAvventuraGrafica.width + shiftX  &&
                       y + img.height <= EditorMappaAvventuraGrafica.height)
                    {
                        img.x = x + shiftX;
                        img.y = y;
                    }
                }
                else
                {
                    //WIDTH/CELLEX 
                    int widthImg = tmp.getImmagine().width / (Integer)(tmp.getImmagine().getSpriteCellsXY()[0]);
                    //HEIGHT/CELLEY
                    int heightImg = tmp.getImmagine().height / (Integer)(tmp.getImmagine().getSpriteCellsXY()[1]);

                    if(x >= 0 && y >= 0 &&
                       x + shiftX + widthImg <= EditorMappaAvventuraGrafica.width + shiftX  &&
                       y + heightImg <= EditorMappaAvventuraGrafica.height)
                    {
                        img.x = x + shiftX;
                        img.y = y;
                    }
                }
                BaseCollisione colltmp = ((BaseCollisione)(tmp.getObjectCollisione()));
                if(colltmp != null)
                {
                    for(int ip=0; ip<colltmp.punti.length; ++ip)
                    {
                        colltmp.punti[ip].x = img.x + colltmp.puntiDistanza[ip].x;
                        colltmp.punti[ip].y = img.y + colltmp.puntiDistanza[ip].y;
                    }
                }
            }
        }
    }
    
    //Sposta ed eventualmente snappa il mouse
    //NOTA: aggiungo 20 pixel perchè il pannello è leggermente spostato
    @Override
    public void mouseMoved(MouseEvent me) {
        //System.out.println(currentwidth);
        if(!this.snapAttivo)
        {
            if(me.getX() + shiftX <= (currentwidth) &&
                    me.getX()>= 0)
                this.x=me.getX();
            
            if(me.getY() <= this.height &&
                    me.getY() >= 0)
                this.y=me.getY();
        }
        else
        {
            //Aggiungo 5px al mousex e 5px al mousey per facilitarne lo snap..
            if((me.getX() + shiftX  < currentwidth + 5)  && me.getX()>=0 &&
                 (me.getY() < this.height + 5)  && me.getY() >= 0)
                snappa(me);
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        /*
        if(MainPanel.strumentolinea)
        {
            xpartenzadisegno = this.x;
            ypartenzadisegno = this.y;
            stodisegnando=true;
            this.requestFocus();
        }*/
    }

    @Override
    public void mousePressed(MouseEvent me) {
        //TASTO CENTRALE (SHIFT SULLA MAPPA)
        if(me.getButton()==2)
        {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
            stoPremendoMouseCentrale=true;
            old_mousex=me.getX();
            this.requestFocus();
        }
        //TASTO SINISTRO: SELEZIONE
        else if(me.getButton()==1)
        {
            //SETTO TUTTI GLI OGGETTI A NON SELEZIONATO
            oggettiSelezionati.clear();
            
            //IMPOSTO TUTTI I VERTICI DELLE COLLISIONI A NON SELEZIONATO
            int numeroOggetti = MainPanel.mappa.getNumeroOggetti();
            for(int i=0; i<numeroOggetti; ++i)
            {
                BaseCollisione tmp = (BaseCollisione)(MainPanel.mappa.popOggetto(i).getObjectCollisione());
                if(tmp != null)
                    tmp.AzzeraSelezionePunti();
            }
            
            Boolean trovatoPuntoSelezionato = false;
            int indexOggetto = 0;
            int indexPuntoCollisione = 0;
            //CERCO UN VERTICE SELEZIONATO
            for(indexOggetto=0; indexOggetto<numeroOggetti && !trovatoPuntoSelezionato; ++indexOggetto)
            {
                Oggetto temp = MainPanel.mappa.popOggetto(indexOggetto);
                String id = temp.id;
                
                //SOLO SE TUTTI GLI OGGETTI O OGGETTO ATTUALMENTE SELEZIONATO SUL DDL
                if(MainPanel.getSelectedObject().toLowerCase().equals("all") || 
                    id.equals(MainPanel.getSelectedObject()))
                {
                    BaseCollisione tmp = (BaseCollisione)(MainPanel.mappa.popOggetto(indexOggetto).getObjectCollisione());
                    if(tmp!= null)
                    {
                        for(indexPuntoCollisione=0; indexPuntoCollisione<tmp.numeroPunti && !trovatoPuntoSelezionato; ++indexPuntoCollisione)
                        {
                            trovatoPuntoSelezionato = 
                                    (me.getX()>= tmp.getPunti()[indexPuntoCollisione].x-5 &&
                                    me.getX() <= tmp.getPunti()[indexPuntoCollisione].x+5 &&
                                    me.getY() >= tmp.getPunti()[indexPuntoCollisione].y-5 &&
                                    me.getY() <= tmp.getPunti()[indexPuntoCollisione].y+5);
                        }   
                    }
                }
            }
            
            --indexOggetto;
            --indexPuntoCollisione;
            
            if(trovatoPuntoSelezionato)
            {
                BaseCollisione tmp = (BaseCollisione)(MainPanel.mappa.popOggetto(indexOggetto).getObjectCollisione());
                tmp.SelezionaPunto(indexPuntoCollisione);
                //System.out.println("SELEZIONATO UN VERTICE! "+indexOggetto+"  "+indexPuntoCollisione);
            }
            
            else
            {
                //CERCO SE SONO DENTRO A UN OGGETTO PER IMPOSTARE LA SELEZIONE
                for(int i=0; i< MainPanel.mappa.getNumeroOggetti();++i)
                {
                    Oggetto temp = MainPanel.mappa.popOggetto(i);
                    String id = temp.id;

                    //SOLO SE TUTTI GLI OGGETTI O OGGETTO ATTUALMENTE SELEZIONATO SUL DDL
                    if(MainPanel.getSelectedObject().toLowerCase().equals("all") || 
                            id.equals(MainPanel.getSelectedObject()))
                    {
                        if(!oggettiSelezionati.contains(id))
                        {
                            //28.06.2014: ADESSO ASCOLTA LARGHEZZA E ALTEZZA IN BASE NUMERO CELLE
                            //SE OGGETTO è ANIMATO.
                            if(!temp.getImmagine().isAnimated)
                            {
                                //CENTRATO UN OGGETTO
                                if(
                                        me.getX() + shiftX >= temp.getImmagine().x &&
                                        me.getY() >= temp.getImmagine().y &&
                                        me.getX() + shiftX <= temp.getImmagine().x + temp.getImmagine().width &&
                                        me.getY() <= temp.getImmagine().y + temp.getImmagine().height
                                )
                                {
                                    oggettiSelezionati.add(id);
                                }
                            }
                            else
                            {
                                int width_ = temp.getImmagine().width / (int)temp.getImmagine().getSpriteCellsXY()[0];
                                int height_ = temp.getImmagine().height / (int)temp.getImmagine().getSpriteCellsXY()[1];
                                //CENTRATO UN OGGETTO
                                if(
                                        me.getX() + shiftX >= temp.getImmagine().x &&
                                        me.getY() >= temp.getImmagine().y &&
                                        me.getX() + shiftX <= temp.getImmagine().x + width_ &&
                                        me.getY() <= temp.getImmagine().y + height_
                                )
                                {
                                    oggettiSelezionati.add(id);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        old_mousex=0;
        stoPremendoMouseCentrale=false;
        
        xpartenzadisegno=0;
        ypartenzadisegno=0;
        stodisegnando=false;
        
        
        this.setCursor(
                getToolkit().createCustomCursor( new BufferedImage( 1, 1, BufferedImage.TYPE_INT_ARGB ), new Point(), null ));
    }
    
    @Override
    public void mouseEntered(MouseEvent me) {
        mousedentro=true;
        this.requestFocus();
    }

    @Override
    public void mouseExited(MouseEvent me) {
        mousedentro=false;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_CONTROL)
        {
            snapAttivo = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        snapAttivo=false;
    }
}