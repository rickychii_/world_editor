package editor.mappa.avventura.grafica;
import java.awt.Image;

import java.util.*;

public class MappaAttuale 
{
    public List<Oggetto> listaOggetti;
    public Image sfondo;
    public String pathsfondo;
    
    public MappaAttuale()
    {
        listaOggetti=new ArrayList<>();
        sfondo = null;
        pathsfondo = "null";
    }
    
    public int getNumeroOggetti()
    {
        return listaOggetti.size();
    }
    
    public Boolean pushOggetto(String id, int zindex)
    {
       return listaOggetti.add(new Oggetto(id, zindex));
    }
    public Oggetto popOggetto(int index)
    {
        if(index >= 0 && index < listaOggetti.size())
        {
            return  listaOggetti.get(index);
        }
        else
        {
            return null;
        }
    }
    //RITORNA l'INDICE IN CUI é INSERITO UNO SPECIFICO OGGETTO
    public int getIndex(String id_oggetto)
    {
        int index = -1;
        Boolean trovato = false;
        int dim = listaOggetti.size();
        
        Oggetto x = null;
        
        for(int i=0; i<dim && !trovato; ++i)
        {
            x =  (Oggetto)(listaOggetti.get(i));
            trovato = x.id == id_oggetto;
            if(trovato)
                index = i;
        }
        return index;
    }
    public Oggetto getOggetto(String id_oggetto)
    {
        //CERCO OGGETTO CON QUEL ID
        boolean trovato = false;
        Oggetto oggettoAttuale = null;
        int i=0;
        while(i<MainPanel.mappa.listaOggetti.size() && !trovato)
        {
            trovato = id_oggetto.equals(listaOggetti.get(i).id);
            if(!trovato)
                ++i;
        }
        if(trovato)
            oggettoAttuale=listaOggetti.get(i);
        return oggettoAttuale;
    }
    //IMPOSTO ID, Zindex e Collisioni..
    public Exception setOggetto(int indice, String id, int zindex, String tipoCollisione)
    {
        try
        {
            Oggetto oggetto = listaOggetti.get(indice);
            oggetto.id=id;
            oggetto.zindex=zindex;
            oggetto.setCollisione(tipoCollisione);
            return null;
        }
        catch(Exception ex)
        {
            return ex;
        }
    }
    //IMPOSTO ID, ZINDEX
    //HO GIA COLLISIONE IN PANCIA
    public Exception setOggetto(int indice, String id, int zindex)
    {
        try
        {
            Oggetto oggetto = listaOggetti.get(indice);
            oggetto.id=id;
            oggetto.zindex=zindex;
            return null;
        }
        catch(Exception ex)
        {
            return ex;
        }
    }
    public boolean esistegiaOggetto(String id)
    {
        boolean esistegia = false;
        for(int i=0; i<listaOggetti.size() && !esistegia; ++i)
        {
            esistegia = (listaOggetti.get(i).id.equals(id));
        }
        return esistegia;
    }
    //NOTA:
    //UNA MAPPA LOCALE CORRISPONDE AD UNA SCENA.
    //UNA MAPPA COMPLETA SU XML COMINCIA CON <map id="parcheggio">
    //E ALL'INTERNO HA PIU SCENE.
    //GESTIRE QUESTA COSA MANUALMENTE.
    //MANCA L'ID DELLA SCENA. GENERARLO QUANDO SI SALVA L'INTERA MAPPA.
    //ULTIMA MODIFICA: 29.06.2014
    public String getScript()
    {
        //INTESTAZIONE
        String script = "<scene id=\"\" background=\"" + pathsfondo + "\" width=\"" + MainPanel.widthMappa + "\">";
        script += "\r\n";
        
        //1 TAB DI INDENTAZIONE
        String indentazione="\t";
        
        //PRENDO TUTTI GLI OGGETTI
        for(int i=0; i<getNumeroOggetti(); ++i)
        {
            Oggetto tmp = listaOggetti.get(i);
            
            script += indentazione + "<object id=\"" + tmp.id + "\" z-index=\"" +  tmp.zindex +"\">";
            script += "\r\n";
            script += indentazione + indentazione + "<img x=\"" + tmp.getImmagine().x + "\" y=\"" + tmp.getImmagine().y + "\" width=\"" + tmp.getImmagine().width
                    + "\" height=\"" + tmp.getImmagine().height + "\"  isanimated=\"" + tmp.getImmagine().isAnimated + "\" filename=\"" + tmp.getImmagine().path + "\" />";
            script += "\r\n";
            
            //COLLISIONE
            BaseCollisione coll = (BaseCollisione)tmp.getObjectCollisione();
            if(null != coll)
            {
                script += indentazione + indentazione + "<collision type=\"" + tmp.getCollisione() + "\">";
                script += "\r\n";
                for(int ip = 0; ip < coll.numeroPunti; ++ip)
                {
                    script += indentazione + indentazione + indentazione + "<point x=\"" + coll.getPunto(ip).x + "\" y=\"" + coll.getPunto(ip).y + "\" />";
                    script += "\r\n";
                }
                script += indentazione + indentazione + "</collision>";
                script += "\r\n";
            }
            
            //ANIMAZIONE
            if(tmp.getImmagine().isAnimated)
            {
                script += indentazione + indentazione + "<animation cellsx=\"" + tmp.getImmagine().getSpriteCellsXY()[0] + "\" cellsy=\"" + tmp.getImmagine().getSpriteCellsXY()[1] + "\">";
                script += "\r\n";
                
                for(int ia = 0; ia < tmp.getImmagine().listaAnimazioni.size(); ++ia)
                {
                    script += indentazione + indentazione + indentazione + "<motion id=\"" + tmp.getImmagine().listaAnimazioni.get(ia).id + "\" ysprite=\"" + tmp.getImmagine().listaAnimazioni.get(ia).yAnimazione + 
                            "\" righe=\"" + tmp.getImmagine().listaAnimazioni.get(ia).righeAnimazione + "\" />";
                    script += "\r\n";
                }
                script += indentazione + indentazione + "</animation>";
                script += "\r\n";
            }
            
            
            script +=  indentazione + "</object>";
            script += "\r\n";
        }
        
        //CONCLUSIONE
        script += "</scene>";
        return script;
    }
}