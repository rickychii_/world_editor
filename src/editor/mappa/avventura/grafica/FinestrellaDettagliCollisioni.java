package editor.mappa.avventura.grafica;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

//FINESTRA SOLO DETTAGLI, NON HO PREVISTO MODIFICA DEI PUNTI DI COLLISIONE.
public class FinestrellaDettagliCollisioni extends JDialog 
{
    //LAVORO SU UN OGGETTO.
    Oggetto target;
    static final long serialVersionUID = 10;
    
    public FinestrellaDettagliCollisioni(Oggetto target, final JFrame sender)
    {
            this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent we) {
                sender.setEnabled(false);
            }

            @Override
            public void windowClosing(WindowEvent we) {
            }

            @Override
            public void windowClosed(WindowEvent we) {
                sender.setEnabled(true);
                sender.requestFocus();
            }

            @Override
            public void windowIconified(WindowEvent we) {
            }

            @Override
            public void windowDeiconified(WindowEvent we) {
            }

            @Override
            public void windowActivated(WindowEvent we) {
            }

            @Override
            public void windowDeactivated(WindowEvent we) {
            }
        });
        
        
        
        this.target=target;
        
        this.setSize(350,280);
        this.setTitle("Collisione oggetto "+target.id);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        BaseCollisione coll = (BaseCollisione)(target.getObjectCollisione());
        
        
        JPanel contenuto = new JPanel(){
            int width=350;
            int height=280;
            
            byte nOggettiY=8;
            
            @Override
            public void paint(Graphics grphcs) {
                super.paint(grphcs); 
                
                grphcs.setColor(new Color(0x9A, 0x9A, 0xAE));
                
                /*
                DISEGNA LE RIGHE MALE DENTRO AL CICLO, NON RISULTANO ESSERE CONFORMI ALLA GRAFICA DEL GENITORE :(
                for(byte i=1; i <= nOggettiY; ++i)
                {
                    grphcs.drawLine(0, deltaHeight * i, width, deltaHeight * i);
                }*/
                
                //MODO MANUALE..
                grphcs.drawLine(0, 32, width, 32);
                grphcs.drawLine(0, 65, width, 65);
                grphcs.drawLine(0, 96, width, 96);
                grphcs.drawLine(0, 127, width, 127);
                grphcs.drawLine(0, 157, width, 157);
                grphcs.drawLine(0, 185, width, 185);
                grphcs.drawLine(0, 220, width, 220);
                grphcs.drawLine(0, 250, width, 250);
            }
        };
        
        contenuto.setLayout(new GridLayout(8, 5));
        
        Integer[][] puntiColl = new Integer[8][2];
        JLabel[][] testoColl = new JLabel[8][2];
        
        byte i=0;
        //C'é UNA COLLISIONE?
        if(null != coll)
        {
            for(; i < coll.getPunti().length; ++i)
            {
                puntiColl[i] = new Integer[]{coll.getPunti()[i].x, coll.getPunti()[i].y};
            }
        }
        //RIEMPIO PUNTI RIMANENTI
        for(; i < 8; ++i)
        {
            puntiColl[i] = new Integer[]{null, null};  
        }
        
        i = 0;
        //AGGIUNGO TUTTI COMPONENTI
        for(; i < 8; ++i)
        {
            testoColl[i] = new JLabel[]{new JLabel(String.valueOf(puntiColl[i][0])), new JLabel(String.valueOf(puntiColl[i][1]))};
            
            testoColl[i][0].setOpaque(true);
            if(testoColl[i][0].getText().toLowerCase().equals("null"))
                testoColl[i][0].setForeground(new Color(0x9A, 0xAA, 0xCE));
            else
                testoColl[i][0].setForeground(new Color(0xCC, 0x88, 0x88));
            testoColl[i][0].setFont(new Font("Arial", Font.ITALIC, 14));
            
            testoColl[i][1].setOpaque(true);
            if(testoColl[i][1].getText().toLowerCase().equals("null"))
                testoColl[i][1].setForeground(new Color(0x9A, 0xAA, 0xCE));
            else
                testoColl[i][1].setForeground(new Color(0xCC, 0x88, 0x88));
            testoColl[i][1].setFont(new Font("Arial", Font.ITALIC, 14));
            
            contenuto.add(new JLabel("Punto " + i));
            contenuto.add(new JLabel("X"));
            contenuto.add(testoColl[i][0]);
            contenuto.add(new JLabel("Y"));
            contenuto.add(testoColl[i][1]);
        }
        
        //CENTRO DELLO SCHERMO!
        this.setLocationRelativeTo(null);
        
                               
        this.add(contenuto);
        this.setVisible(true);
    }
}