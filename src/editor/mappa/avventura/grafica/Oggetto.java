package editor.mappa.avventura.grafica;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.ArrayList;

public class Oggetto {
    
    public String id;
    public int zindex;
    
    private ImmagineOggetto immagine;
    private Object collisione;
    
    
    public Oggetto(String id, int zindex)
    {
        this.id=id;
        this.zindex=zindex;
        //CREO UN IMMAGINE "ZOMBIE" NULL
        immagine = new ImmagineOggetto();
    }
    public void setImmagine(BufferedImage img, String nome)
    {
        this.immagine = new ImmagineOggetto(img, nome);
    }
    public void setImmagine(ImmagineOggetto tmp)
    {
        this.immagine =tmp;
    }
    public ImmagineOggetto getImmagine()
    {
        return this.immagine;
    }
    //INIZIALIZZO PUNTI DELLA FIGURA DI COLLISIONE...
    //TENGO CONTO ANCHE DELL'ANIMAZIONE.. SE è ATTIVA DISEGNO
    //FIGURA IN BASE WIDTH, HEIGHT DELLA SOTTOIMMAGINE.
    //NOTA: NON CHIAMARE QUESTO METODO SE IMMAGINE è NULL!
    public void inizializzaFigura()
    {
        int tempWidth = immagine.width;
        int tempHeight = immagine.height;
        
        if(immagine.isAnimated)
        {
            //WIDTH/CELLEX 
            //HEIGHT/CELLEY
            tempWidth = immagine.width / (Integer)(immagine.getSpriteCellsXY()[0]);
            tempHeight = immagine.height / (Integer)(immagine.getSpriteCellsXY()[1]);
        }
        {
            if(collisione instanceof CollisioneA)
            {
                ((CollisioneA)collisione).disegnaFigura(tempWidth, tempHeight, immagine.x, immagine.y);
            }
            else if(collisione instanceof CollisioneB)
            {
                ((CollisioneB)collisione).disegnaFigura(tempWidth, tempHeight, immagine.x, immagine.y);
            }
            else if(collisione instanceof CollisioneC)
            {
                ((CollisioneC)collisione).disegnaFigura(tempWidth, tempHeight, immagine.x, immagine.y);
            }
            else if(collisione instanceof CollisioneD)
            {
                ((CollisioneD)collisione).disegnaFigura(tempWidth, tempHeight, immagine.x, immagine.y);
            }
            else if(collisione instanceof CollisioneE)
            {
                ((CollisioneE)collisione).disegnaFigura(tempWidth, tempHeight, immagine.x, immagine.y);
            }
            else if(collisione instanceof CollisioneF)
            {
                ((CollisioneF)collisione).disegnaFigura(tempWidth, tempHeight, immagine.x, immagine.y);
            }
        }
    }
    public Object getObjectCollisione()
    {
        return collisione;
    }
    public void setCollisione(String type)
    {
        switch(type)
        {
            case "A":
                collisione = new CollisioneA();
                break;
            case "B": 
                collisione = new CollisioneB();
                break;
            case "C": 
                collisione = new CollisioneC();
                break;
            case "D": 
                collisione = new CollisioneD();
                break;
            case "E": 
                collisione = new CollisioneE();
                break;
            case "F": 
                collisione = new CollisioneF();
                break;    
            default:
                collisione = null;
        }
    }
    public String getCollisione()
    {
        if(collisione instanceof CollisioneA)
            return "A";
        if(collisione instanceof CollisioneB)
            return "B";
        if(collisione instanceof CollisioneC)
            return "C";
        if(collisione instanceof CollisioneD)
            return "D";
        if(collisione instanceof CollisioneE)
            return "E";
        if(collisione instanceof CollisioneF)
            return "F";
        else
            return "n";
    }
}

final class ImmagineOggetto extends Thread
{
    //IMMAGINE HA TUTTE VARIABILI public
    public String path;
    public int x;
    public int y;
    public int width;
    public int height;
    public boolean isAnimated;
    public List<AnimazioneOggetto> listaAnimazioni;
    
    //CELLEX, CELLEY
    private Integer spriteWidth;
    private Integer spriteHeight;
    
    //IMMAGINE (EVENTUALMENTE SUBIMAGE)
    public BufferedImage img;
    
    //VARIABILI PER ANIMAZIONE
    //ANIMAZIONI A 10FPS
    private final byte fpsAnimazione = 10;
    //i righe
    //j colonne 
    private int i;
    private int j;

    
    //PRENDO IL FRAME CORRENTE
    public BufferedImage getFrame()
    {
        if(img == null || spriteHeight == null || spriteWidth == null)
            return null;
        
        else
            return
                img.getSubimage(
                    i * (width / spriteWidth),
                    j * (height / spriteHeight),    
                     (width / spriteWidth),
                     (height / spriteHeight));
    }
    @Override
    public void run()
    {
        //PRENDO ANIMAZIONE SELEZIONATA
        AnimazioneOggetto anim = getAnimazioneSelezionata();
        
        int nrighe = anim.righeAnimazione;
        
        while(MainPanel.stoAnimando)
        {
            //RIGA 0
            i = 0;
            
            //J è RIGA ATTUALE DI ANIMAZIONE
            j = anim.yAnimazione;
            
            //J VA DA YANIM A YANIM + NRIGHE
            while(j < anim.yAnimazione + nrighe)
            {       
                while(i < spriteWidth)
                {
                    //System.out.println(i+"  "+j);
                    try
                    {
                        Thread.sleep(1000 / fpsAnimazione);
                    }
                    catch(Exception e)
                    {
                        System.err.println("ERRORE THREAD SPRITES "+e.toString());
                    }
                    ++i;
                    if(MainPanel.stoAnimando == false)
                        break;
                }
                i = 0;
                ++j;
                if(MainPanel.stoAnimando == false)
                    break;
            }
        }
        i=0;
        j=0;
    }
    
    //COSTRUTTORE CHE CREA UN OGGETTO SENZA IMMAGINE.
    public ImmagineOggetto()
    {
        img = null;
        spriteWidth = null;
        spriteHeight = null;
    }
    public ImmagineOggetto(BufferedImage img_, String nomeFile)
    {
        this.path = EditorMappaAvventuraGrafica.percorsoImmagini + nomeFile;
        loadImg(img_);
    }
    public boolean RimuoviAnimazione (String id)
    {
        boolean fatto = false;
        if(listaAnimazioni.size()==1)
            return false;
        AnimazioneOggetto ret = null;
        for(int i=0; i<listaAnimazioni.size(); ++i)
            if(listaAnimazioni.get(i).id.equals(id))
            {
                ret = listaAnimazioni.get(i);
                listaAnimazioni.remove(ret);
                fatto = true;
            }
        return fatto;
    }
    public AnimazioneOggetto getAnimazione(String id)
    {
        AnimazioneOggetto ret = null;
        for(int i=0; i<listaAnimazioni.size(); ++i)
        {
            if(listaAnimazioni.get(i).id.equals(id))
            {
                ret = listaAnimazioni.get(i);
                break;
            }
        }
        return ret;
    }
    //RICERCA NON CASE-SENSITIVE!
    public boolean containsAnimation(String _id)
    {
        boolean ub = false;
        for(int i=0; i<listaAnimazioni.size(); ++i)
        {
            if(listaAnimazioni.get(i).id.toLowerCase().equals(_id.toLowerCase()))
            {
                ub = true;
                break;
            }
        }
        return ub;
    }
    public void resettaAnimazioni()
    {
        listaAnimazioni = new ArrayList<AnimazioneOggetto>();
        listaAnimazioni.add(new AnimazioneOggetto("Animazione0", 0, 1));
        
        //IMPOSTO L'UNICA ANIMAZIONE A SELEZIONATA 28.06.2014
        listaAnimazioni.get(0).eSelezionata = true;
    }
    public void AzzeraAnimazioni()
    {
        listaAnimazioni=null;
    }
    public int getIndiceAnimazione(String idAnimazione)
    {
        int index = -1;
        for(int i=0; i<listaAnimazioni.size(); ++i)
        {
            if(i == index)
            {
                i = index;
                break;
            }
        }
        return index;
    }
    public boolean selezionaAnimazione(int indice)
    {
        if(indice < 0 || indice >= listaAnimazioni.size())
            return false;
        for(int i=0; i<listaAnimazioni.size(); ++i)
        {
            if(indice == i)
                listaAnimazioni.get(i).eSelezionata = true;
            else
                listaAnimazioni.get(i).eSelezionata = false;
        }
        return true;
    }
    public boolean selezionaAnimazione(String id)
    {
        boolean ret = false;
        for(int i=0; i<listaAnimazioni.size(); ++i)
        {
            if(id.equals(listaAnimazioni.get(i).id))
            {
                listaAnimazioni.get(i).eSelezionata = true;
                ret = true;
            }
            else
            {
                listaAnimazioni.get(i).eSelezionata = false;
            }
        }
        return true;
    }
    public AnimazioneOggetto getAnimazioneSelezionata()
    {
        AnimazioneOggetto ret = null;
        
        if(null == listaAnimazioni)
            return ret;
        
        for(int i=0; i<listaAnimazioni.size(); ++i)
        {
            if(listaAnimazioni.get(i).eSelezionata)
            {
                ret = listaAnimazioni.get(i);
                break;
            }
        }
        return ret;
    }
    public void inizializzaWidthHeightSprite()
    {
        //W,H LAVORANO IN COPPIA.
        if(spriteWidth == null && spriteHeight == null)
        {
            spriteWidth = width;
            spriteHeight = height;
        }
    }
    public void setSpriteCellsXY(int w, int h)
    {
        spriteWidth = w;
        spriteHeight = h;
    }
    //SPRITEWIDTH = CELLE X
    //SPRITEHEIGHT = CELLE Y
    public Object[] getSpriteCellsXY()
    {
        //W, H LAVORANO IN COPPIA
        if(spriteWidth != null && spriteHeight != null)
            return new Object[]{spriteWidth, spriteHeight};
        else
            return new Object[]{0, 0};
    }
    
    //CARICO IMMAGINE E ANCHE VARIABILI ASSOCIATE:
    public void loadImg(BufferedImage img_)
    {
        this.img = img_;
        
        x=0;
        y=0;
        width=img_.getWidth();
        height=img_.getHeight();
        
        isAnimated=false;
    }
}
//NELLA CLASSE IMMAGINI OGGETTO GESTISCO UN ARRAY DI ANIMAZIONI.
//OGNI ARRAY CORRISPONDE AD UNA SPECIFICA ANIMAZIONE.
final class AnimazioneOggetto
{
    public String id;
    public int yAnimazione;
    public int righeAnimazione;
    
    //NON VA REGISTRATO SUL FILE.
    //SERVE SOLO SU PROGRAMMA LOCALE PER FAR PARTIRE L'ANIMAZIONE.
    public boolean eSelezionata = false;
    
    public AnimazioneOggetto(String id, int yAnimazione, int righeAnimazione)
    {
        this.id = id;
        this.yAnimazione = yAnimazione;
        this.righeAnimazione = righeAnimazione;
    }
}

abstract class BaseCollisione
{
    //VETTORI DI PUNTI CHE COMPONGONO IL POLIGONO DI COLLISIONE
    Point[] punti;
    
    //VETTORE DI DISTANZE: MI SERVE PER SPOSTARE IL POLIGONO DI COLLISIONE ASSIEME ALL'IMMAGINE
    //DISTANZA RISPETTO TOP LEFT.
    Point[] puntiDistanza;
    
    //VETTORE DI PUNTI SELEZIONATI
    Boolean[] PuntoSelezionato;
    
    int numeroPunti;

    public BaseCollisione(int numeroPunti)
    {
        this.numeroPunti = numeroPunti;
        
        this.punti = new Point[numeroPunti];
        this.puntiDistanza = new Point[numeroPunti];
        this.PuntoSelezionato = new Boolean[numeroPunti];
        
        
        for(int i=0;i<numeroPunti;++i)
        {
            punti[i] = new Point();
            puntiDistanza[i] = new Point();
            PuntoSelezionato[i] = false;
        }
    }
    
    //IMPOSTA UN PUNTO (XY) IN BASE AD UN INDICE.
    //SE INDICE SBAGLIATO RITORNA FALSE.
    //PASSO TOP, LEFT PER CALCOLARE DISTANZE @_@
    public Boolean setPunto(int index, int x, int y, int posLeftFigura, int posTopFigura)
    {
        Boolean fatto = false;
        if(index >=0 && index < numeroPunti)
        {
            punti[index] = new Point(x, y);
            puntiDistanza[index] = new Point(punti[index].x - posLeftFigura, punti[index].y - posTopFigura);
            fatto = true;
        }
        
        return fatto;
    }
    
    public void AzzeraSelezionePunti()
    {
        for(int i=0;i<numeroPunti;++i)
        {
            PuntoSelezionato[i] = false;
        }
    }
    
    public Boolean almenounpuntoselezionato()
    {
        Boolean selezionato = false;
        
        for(int i=0; i<numeroPunti && ! selezionato; ++i)
            selezionato=PuntoSelezionato[i] == true; 
        
        return selezionato;
    }
    
    //RESTITUISCE INDICE PUNTO SELEZIONATO.
    //SE NESSUN PUNTO SELEZIONATO RITORNA -1.
    public int getIndexPuntoSelezionato()
    {
        int index = 0;
        Boolean selezionato=false;
        
        for(; index<numeroPunti && ! selezionato; ++index)
        {
            selezionato=PuntoSelezionato[index] == true;
        }
        
        if(!selezionato)
            return -1;
        else
        {
            --index;
            return index;
        }
    }
    
    public void SelezionaPunto(int indice)
    {
        if(indice>=0 && indice<numeroPunti)
            PuntoSelezionato[indice] = true;
    }
    
    //METODO PER INIZIALIZZARE I PUNTI (LA FIGURA) 
    //DATE WIDTH & HEIGHT
    //UNA FIGURA SARA' SEMPRE 60% DELLE DIMENSIONI DELL'OGGETTO
    abstract void disegnaFigura(int width, int height, int posx, int posy);
    
    public Point[] getPunti()
    {
        return punti;
    }
    
    public Point getPunto(int index)
    {
        if(index >=0 && index < numeroPunti)
        {
            return punti[index];
        }
        else
        {
            return new Point(-999,-999);
        }
    }
    
    public void spostaPunto(int index, int posx, int posy, int posTopFigura, int posLeftFigura)
    {
        if(index >=0 && index < numeroPunti)
        {
            punti[index].x = posx;
            punti[index].y = posy;
            
            puntiDistanza[index] = new Point(punti[index].x - posLeftFigura, punti[index].y - posTopFigura);
        }
    }
}


//COLLISIONE CON 3 PUNTI
class CollisioneA extends BaseCollisione
{
    public CollisioneA()
    {
        super(3);
    }
    @Override
    public void disegnaFigura(int width, int height, int posx, int posy)
    {
        //DEVO DISEGNARE UN TRIANGOLO
        //     0
        //     .
        //    . .
        //  .     .
        // .........
        //1          2

        int widthBase = (int)(width * 0.6);
        int heightBase = (int)(height * 0.6);
        
        Point punto0 = new Point((width - widthBase)/2 + posx + widthBase / 2, (height - heightBase) / 2 + posy);
        Point punto1 = new Point((width - widthBase)/2 + posx, (height - heightBase) / 2 + posy + heightBase);
        Point punto2 = new Point((width - widthBase)/2 + posx + widthBase, (height - heightBase) / 2 + posy + heightBase);
        
        
        this.punti[0] = punto0;
        this.punti[1] = punto1;
        this.punti[2] = punto2;
        
        this.puntiDistanza[0] = new Point(punto0.x-posx, punto0.y-posy);
        this.puntiDistanza[1] = new Point(punto1.x-posx, punto1.y-posy);
        this.puntiDistanza[2] = new Point(punto2.x-posx, punto2.y-posy);
    }
}
//COLLISIONE CON 4 PUNTI
class CollisioneB extends BaseCollisione
{
    public CollisioneB()
    {
        super(4);
    }
    @Override
    public void disegnaFigura(int width, int height, int posx, int posy)
    {
        //DEVO DISEGNARE UN QUADRATO / RETTANGOLO
        int widthRettangolo = (int)(width * 0.6);
        int heightRettangolo = (int)(height * 0.6);
        
        //0------------1
        //|            |
        //|            |
        //|            |
        //3------------2
        
        Point punto0 = new Point((width - widthRettangolo)/2 + posx, (height - heightRettangolo)/2 + posy);
        Point punto1 = new Point((width - widthRettangolo)/2 + widthRettangolo + posx, (height - heightRettangolo)/2 + posy);        
        Point punto2 = new Point((width - widthRettangolo)/2 + widthRettangolo + posx , (height - heightRettangolo)/2 + heightRettangolo + posy);
        Point punto3 = new Point((width - widthRettangolo)/2 + posx, (height - heightRettangolo)/2 + heightRettangolo + posy);
        
        this.punti[0] = punto0;
        this.punti[1] = punto1;
        this.punti[2] = punto2;
        this.punti[3] = punto3;
        
        this.puntiDistanza[0] = new Point(punto0.x-posx, punto0.y-posy);
        this.puntiDistanza[1] = new Point(punto1.x-posx, punto1.y-posy);
        this.puntiDistanza[2] = new Point(punto2.x-posx, punto2.y-posy);
        this.puntiDistanza[3] = new Point(punto3.x-posx, punto3.y-posy);           
    }
}
//COLLISIONE CON 5 PUNTI
class CollisioneC extends BaseCollisione
{
    public CollisioneC()
    {
        super(5);
    }
    @Override
    public void disegnaFigura(int width, int height, int posx, int posy)
    {
        //DEVO DISEGNARE UN PENTAGONO        
        //              0
        //        
        //      1               4
        //
        //
        //          2       3
        
        //PROVO A SETTARE I 5 LATI IMPOSTANDO 360° / 5
        
        //E PRENDENDO I PUNTI X,Y CON SENO E COSENO
        Point puntoCentrale = new Point(posx + width / 2, posy + height /2);
        int raggio = (int)(width * 0.3);
        int deltaGradi = (int)(360 / 5);
        
        
        Point punto0 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 0)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 0)) * raggio + puntoCentrale.y));
        Point punto1 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 1)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 1)) * raggio + puntoCentrale.y));
        Point punto2 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 2)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 2)) * raggio + puntoCentrale.y));
        Point punto3 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 3)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 3)) * raggio + puntoCentrale.y));
        Point punto4 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 4)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 4)) * raggio + puntoCentrale.y));
        
        this.punti[0] = punto0;
        this.punti[1] = punto1;
        this.punti[2] = punto2;
        this.punti[3] = punto3;        
        this.punti[4] = punto4;
        
        this.puntiDistanza[0] = new Point(punto0.x - posx, punto0.y - posy);
        this.puntiDistanza[1] = new Point(punto1.x - posx, punto1.y - posy);
        this.puntiDistanza[2] = new Point(punto2.x - posx, punto2.y - posy);
        this.puntiDistanza[3] = new Point(punto3.x - posx, punto3.y - posy);
        this.puntiDistanza[4] = new Point(punto4.x - posx, punto4.y - posy);
                                        
    }
}
//COLLISIONE CON 6 PUNTI
class CollisioneD extends BaseCollisione
{
    public CollisioneD()
    {
        super(6);
    }
    @Override
    public void disegnaFigura(int width, int height, int posx, int posy)
    {
        //DEVO DISEGNARE UN ESAGONO
        int widthBase = (int)(width * 0.6);
        int heightBase = (int)(height * 0.6);
        
        //         0    5
        //        
        //     1            4
        // 
        //         2    3
        
        
        
                
        Point punto0 = new Point((width - widthBase)/2 + posx + widthBase / 4, (height - heightBase) / 2 + posy);
        Point punto1 = new Point((width - widthBase)/2 + posx, (height - heightBase)/2 + posy + heightBase/2);  
        Point punto2 = new Point((width - widthBase)/2 + posx + widthBase / 4, (height - heightBase)/2 +posy + heightBase);
        Point punto3 = new Point((width - widthBase)/2 + posx + widthBase / 4 * 3, (height - heightBase)/ 2 + posy + heightBase );
        Point punto4 = new Point((width - widthBase)/2 + posx + widthBase, (height - heightBase)/ 2 + posy + heightBase /2 );
        Point punto5 = new Point((width - widthBase)/2 + posx + widthBase / 4 * 3, (height - heightBase)/ 2 + posy);
        
        this.punti[0] = punto0;
        this.punti[1] = punto1;
        this.punti[2] = punto2;
        this.punti[3] = punto3;
        this.punti[4] = punto4;   
        this.punti[5] = punto5;
        
        
        this.puntiDistanza[0] = new Point(punto0.x - posx, punto0.y - posy);
        this.puntiDistanza[1] = new Point(punto1.x - posx, punto1.y - posy);
        this.puntiDistanza[2] = new Point(punto2.x - posx, punto2.y - posy);
        this.puntiDistanza[3] = new Point(punto3.x - posx, punto3.y - posy);
        this.puntiDistanza[4] = new Point(punto4.x - posx, punto4.y - posy);
        this.puntiDistanza[5] = new Point(punto5.x - posx, punto5.y - posy);
    }
}
//COLLISIONE CON 7 PUNTI
class CollisioneE extends BaseCollisione
{
    public CollisioneE()
    {
        super(7);
    }
    @Override
    public void disegnaFigura(int width, int height, int posx, int posy)
    {
        //DEVO DISEGNARE UNA FIGURA REGOLARE A 7 LATI
        //        3
        //  4               2    
        //
        // 5                   1         
        //                    0
        //
        //      6
        
        //PROVO A SETTARE I 7 LATI IMPOSTANDO 360° / 7
        //E PRENDENDO I PUNTI X,Y CON SENO E COSENO
        Point puntoCentrale = new Point(posx + width / 2, posy + height /2);
        int raggio = (int)(width * 0.3);
        int deltaGradi = (int)(360 / 7);
        
        
        
        Point punto0 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 0)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 0)) * raggio + puntoCentrale.y));
        Point punto1 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 1)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 1)) * raggio + puntoCentrale.y));
        Point punto2 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 2)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 2)) * raggio + puntoCentrale.y));
        Point punto3 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 3)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 3)) * raggio + puntoCentrale.y));
        Point punto4 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 4)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 4)) * raggio + puntoCentrale.y));
        Point punto5 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 5)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 5)) * raggio + puntoCentrale.y));
        Point punto6 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 6)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 6)) * raggio + puntoCentrale.y));
        
        this.punti[0] = punto0;
        this.punti[1] = punto1;
        this.punti[2] = punto2;
        this.punti[3] = punto3;        
        this.punti[4] = punto4;
        this.punti[5] = punto5;
        this.punti[6] = punto6;
        
        
        this.puntiDistanza[0] = new Point(punto0.x - posx, punto0.y - posy);
        this.puntiDistanza[1] = new Point(punto1.x - posx, punto1.y - posy);
        this.puntiDistanza[2] = new Point(punto2.x - posx, punto2.y - posy);
        this.puntiDistanza[3] = new Point(punto3.x - posx, punto3.y - posy);
        this.puntiDistanza[4] = new Point(punto4.x - posx, punto4.y - posy);
        this.puntiDistanza[5] = new Point(punto5.x - posx, punto5.y - posy);
        this.puntiDistanza[6] = new Point(punto6.x - posx, punto6.y - posy);
    }
}
//COLLISIONE CON 8 PUNTI
class CollisioneF extends BaseCollisione
{
    public CollisioneF()
    {
        super(8);
    }
    @Override
    public void disegnaFigura(int width, int height, int posx, int posy)
    {
        //DEVO DISEGNARE UNA FIGURA REGOLARE A 8 LATI
        //        3
        //  4               2    
        //
        // 5                   1         
        //                    0
        //
        //      6       7
        
        //PROVO A SETTARE 8 LATI IMPOSTANDO 360° / 8
        //E PRENDENDO I PUNTI X,Y CON SENO E COSENO
        Point puntoCentrale = new Point(posx + width / 2, posy + height /2);
        int raggio = (int)(width * 0.3);
        int deltaGradi = (int)(360 / 8);
        
        
        
        Point punto0 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 0)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 0)) * raggio + puntoCentrale.y));
        Point punto1 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 1)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 1)) * raggio + puntoCentrale.y));
        Point punto2 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 2)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 2)) * raggio + puntoCentrale.y));
        Point punto3 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 3)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 3)) * raggio + puntoCentrale.y));
        Point punto4 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 4)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 4)) * raggio + puntoCentrale.y));
        Point punto5 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 5)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 5)) * raggio + puntoCentrale.y));
        Point punto6 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 6)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 6)) * raggio + puntoCentrale.y));
        Point punto7 = new Point((int)(Math.sin(Math.toRadians(deltaGradi * 7)) * raggio + puntoCentrale.x), (int)(Math.cos(Math.toRadians(deltaGradi * 7)) * raggio + puntoCentrale.y));
        
        this.punti[0] = punto0;
        this.punti[1] = punto1;
        this.punti[2] = punto2;
        this.punti[3] = punto3;        
        this.punti[4] = punto4;
        this.punti[5] = punto5;
        this.punti[6] = punto6;
        this.punti[7] = punto7;
        
        this.puntiDistanza[0] = new Point(punto0.x - posx, punto0.y - posy);
        this.puntiDistanza[1] = new Point(punto1.x - posx, punto1.y - posy);
        this.puntiDistanza[2] = new Point(punto2.x - posx, punto2.y - posy);
        this.puntiDistanza[3] = new Point(punto3.x - posx, punto3.y - posy);
        this.puntiDistanza[4] = new Point(punto4.x - posx, punto4.y - posy);
        this.puntiDistanza[5] = new Point(punto5.x - posx, punto5.y - posy);
        this.puntiDistanza[6] = new Point(punto6.x - posx, punto6.y - posy);
        this.puntiDistanza[7] = new Point(punto7.x - posx, punto7.y - posy);
    }
}