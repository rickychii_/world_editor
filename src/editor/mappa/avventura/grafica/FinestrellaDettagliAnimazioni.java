package editor.mappa.avventura.grafica;

import java.awt.*;
import java.awt.event.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import javax.swing.*;


public class FinestrellaDettagliAnimazioni extends JDialog implements Runnable
{
    static final long serialVersionUID = 10;

    Oggetto target;
    
    JComboBox ComboAnimazioni;
    
    JButton addAnimazione;
    JButton deleteAnimazione;
    
    JTextField _nomeAnimazione;
    
    JTextField _Yanim;
    JTextField _Righe;
    
    JButton salva;
    JButton chiudi;
    
    JCheckBox CBselezionata;
    
    //VARIABILI PER GESTIRE THREAD
    String idSelezionato;
    boolean finestraAperta;
    
    public FinestrellaDettagliAnimazioni(Oggetto target, final JFrame sender)
    {
        this.target=target;
        
        this.setSize(350, 220);
        
        this.setTitle("Dettagli Animazioni oggetto " + target.id);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        
        //CENTRO DELLO SCHERMO!
        this.setLocationRelativeTo(null);

        this.setLayout(new BorderLayout());
        
        JPanel jpInterno = new JPanel();
        jpInterno.setLayout(new GridLayout(6, 2));
        
        
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent we) {
                sender.setEnabled(false);
            }

            @Override
            public void windowClosing(WindowEvent we) {
                finestraAperta = false;
            }

            @Override
            public void windowClosed(WindowEvent we) {
                sender.setEnabled(true);
                sender.requestFocus();
            }

            @Override
            public void windowIconified(WindowEvent we) {
            }

            @Override
            public void windowDeiconified(WindowEvent we) {
            }

            @Override
            public void windowActivated(WindowEvent we) {
            }

            @Override
            public void windowDeactivated(WindowEvent we) {
            }
        });
        
        
        
        //JLabel jlTutteAnimazioni = new JLabel("Animazioni disponibili per l'oggetto");
        
        ComboAnimazioni = new JComboBox();
        //ComboAnimazioni.addItem("PROVA");
        
        
        List<AnimazioneOggetto> arrayAnimazioni = target.getImmagine().listaAnimazioni;
        if(arrayAnimazioni != null)
        {
            for(int i=0; i<arrayAnimazioni.size(); ++i)
            {
                ComboAnimazioni.addItem(((AnimazioneOggetto)(arrayAnimazioni.get(i))).id);
            }
        }
        
        //28.06.2014: DA PROBLEMI NEL PAINT DEL PANNELLO IMMAGINE
        //NULL POINTER EXCEPTION :/
        /*
        ComboAnimazioni.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent arg0) {
                
                //JOptionPane.showMessageDialog(null, "PASSO QUI","Ric.C", JOptionPane.OK_OPTION);
                
                JComboBox jcb = (JComboBox)(arg0.getSource());
                if(! (jcb.getSelectedItem() == null))
                {
                    FinestrellaDettagliAnimazioni fin = (FinestrellaDettagliAnimazioni)(jcb.getParent().getParent().getParent().getParent());
                    Oggetto target = fin.target;
                    String animazioneSelezionata = jcb.getSelectedItem().toString();
                    target.id = animazioneSelezionata;
                    //JOptionPane.showMessageDialog(null, animazioneSelezionata,"Ric.C", JOptionPane.OK_OPTION);
                    
                    BindaDatiAnimazione();
                }
            }
        });*/
        
        addAnimazione = new JButton("+ ADD");
        deleteAnimazione = new JButton("- DELETE");
        
        this.add(ComboAnimazioni, BorderLayout.NORTH);
        
        
        addAnimazione.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)(ae.getSource());
                FinestrellaDettagliAnimazioni fin = (FinestrellaDettagliAnimazioni)(jb.getParent().getParent().getParent().getParent().getParent());
                
                int totAnimazioni = fin.target.getImmagine().listaAnimazioni.size();
                String nuovaAnimazione = "Animazione" + totAnimazioni;
                
                if(!(fin.target.getImmagine().containsAnimation(nuovaAnimazione)))
                {
                    fin.target.getImmagine().listaAnimazioni.add(new AnimazioneOggetto(nuovaAnimazione, 0, 1));
                    fin.databindJcombo(totAnimazioni);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "ID Animazione gia presente!","Ric.C", JOptionPane.OK_OPTION);
                }
            }
        });
        
        deleteAnimazione.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)(ae.getSource());
                FinestrellaDettagliAnimazioni fin = (FinestrellaDettagliAnimazioni)(jb.getParent().getParent().getParent().getParent().getParent());
                String toRemove = (String)(fin.ComboAnimazioni.getSelectedItem());
                
                fin.target.getImmagine().RimuoviAnimazione(toRemove);
                int totAnimazioni = fin.target.getImmagine().listaAnimazioni.size();
                
                fin.databindJcombo(totAnimazioni - 1);
            }
        });
        
        jpInterno.add(addAnimazione);
        jpInterno.add(deleteAnimazione);
        
        jpInterno.add(new JLabel("Y Partenza"));
        jpInterno.add(new JLabel("Righe Animazione"));
        
        
        _Yanim = new JTextField();
        _Righe = new JTextField();
        _Yanim.setDocument(new JTextFieldLimit(3));
        _Righe.setDocument(new JTextFieldLimit(3));
        
        
        jpInterno.add(_Yanim);
        jpInterno.add(_Righe);
        
        
        _nomeAnimazione = new JTextField();
        _nomeAnimazione.setDocument(new JTextFieldLimit(15));
        
        
        jpInterno.add(new JLabel("Nome"));
        jpInterno.add(_nomeAnimazione);
           
        jpInterno.add(new JLabel("Imposta selezionata"));
        CBselezionata = new JCheckBox();
        
        jpInterno.add(CBselezionata);
        
        salva = new JButton("Salva");
        chiudi = new JButton("Chiudi");
        
        salva.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)(ae.getSource());
                FinestrellaDettagliAnimazioni fin = (FinestrellaDettagliAnimazioni)(jb.getParent().getParent().getParent().getParent().getParent());
                
                //DATI
                String nuovoyAnim = fin._Yanim.getText();
                String nuovorighe = fin._Righe.getText();
                String nuovoID = fin._nomeAnimazione.getText();
                
                //GESTISCO ECCEZIONI PER I VALORI YANIM E TOTRIGHE
                int cellex = (int)(fin.target.getImmagine().getSpriteCellsXY()[0]);
                int celley = (int)(fin.target.getImmagine().getSpriteCellsXY()[1]);
                
                int intnuovoyAnim = Integer.parseInt(nuovoyAnim);
                int intnuovorighe = Integer.parseInt(nuovorighe);
                
                //GESTISCO EVENTUALI ECCEZIONI
                if(intnuovoyAnim + intnuovorighe > celley || intnuovorighe == 0)
                {
                    JOptionPane.showMessageDialog(null, "Errore nei dati","Ric.C", JOptionPane.OK_OPTION);
                }
                
                
                //NUOVO ID E IDINPANCIA SONO DIVERSI: SE NON GIA PRESENTE NELLA LISTA ANIMAZIONI SALVO IL NUOVO ID
                else if(!(idSelezionato.equals(nuovoID)))
                {
                    if(!(fin.target.getImmagine().containsAnimation(nuovoID)))
                    {
                        //PRENDO ANIMAZIONE INTERESSATA
                        String idAnimazioneinPancia = ComboAnimazioni.getSelectedItem().toString();
                        AnimazioneOggetto AnimazioneinPancia = fin.target.getImmagine().getAnimazione(idAnimazioneinPancia);

                        //AGGIORNO
                        AnimazioneinPancia.id = nuovoID;
                        AnimazioneinPancia.righeAnimazione = Integer.parseInt(nuovorighe);
                        AnimazioneinPancia.yAnimazione = Integer.parseInt(nuovoyAnim);

                        //SE SERVE IMPOSTO A ANIMAZIONE - SELEZIONATA
                        if(fin.CBselezionata.isSelected())
                        {
                            fin.target.getImmagine().selezionaAnimazione(nuovoID);
                        }

                        //DATABIND
                        fin.databindJcombo(nuovoID);
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "ID Animazione gia presente!","Ric.C", JOptionPane.OK_OPTION);
                    }
                }
                //NUOVO ID E ID IN PANCIA SONO UGUALI: NON SALVO ID
                else
                {
                    //PRENDO ANIMAZIONE INTERESSATA
                    String idAnimazioneinPancia = ComboAnimazioni.getSelectedItem().toString();
                    AnimazioneOggetto AnimazioneinPancia = fin.target.getImmagine().getAnimazione(idAnimazioneinPancia);

                    //AGGIORNO
                    AnimazioneinPancia.righeAnimazione = Integer.parseInt(nuovorighe);
                    AnimazioneinPancia.yAnimazione = Integer.parseInt(nuovoyAnim);
                    //SE SERVE IMPOSTO A ANIMAZIONE - SELEZIONATA
                    if(fin.CBselezionata.isSelected())
                    {
                        fin.target.getImmagine().selezionaAnimazione(nuovoID);
                    }

                    //DATABIND
                    fin.databindJcombo(nuovoID);
                }
            }
        });
        
        chiudi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JButton jb = (JButton)(ae.getSource());
                FinestrellaDettagliAnimazioni fin = (FinestrellaDettagliAnimazioni)(jb.getParent().getParent().getParent().getParent().getParent());
                fin.dispose();
            }
        });
        
        jpInterno.add(salva);
        jpInterno.add(chiudi);
        
        this.add(jpInterno, BorderLayout.CENTER);
        
        
        
        this.setVisible(true);
        
        //THREAD PER GESTIRE ELEMENTI SELEZIONATI DA JCOMBOBOX
        idSelezionato = ComboAnimazioni.getSelectedItem().toString();
        finestraAperta = true;
        new Thread(this).start();
        
        BindaDatiAnimazione();
    }
    
    @Override
    public void run()
    {
        while(finestraAperta)
        {
            //System.out.println(idSelezionato);
            if(!(ComboAnimazioni.getSelectedItem().toString().equals(idSelezionato)))
            {   
                //CAMBIO ANIMAZIONE SELEZIONATA
                CambiaAnimazioneSelezionata(ComboAnimazioni.getSelectedItem().toString());
            }
            try
            {
                Thread.sleep(15);
            }
            catch(Exception ex)
            {
                System.err.println(ex.toString());
            }
        }
    }
    
    private void CambiaAnimazioneSelezionata(String id)
    {
        //SETTO OGGETTO ATTUALMENTE SELEZIONATO
        idSelezionato = id;
        
        //BINDO I DATI DELL'ANIMAZIONE ATTUALE SUL COMBOBOX
        databindJcombo(idSelezionato);
        
        //BINDO I DATI DELL'ANIMAZIONE ATTUALE NELLA FINESTRA PADRE
        BindaDatiAnimazione();
    }            
    
    private void BindaDatiAnimazione()
    {
        String idAnimazioneinPancia = idSelezionato;
        AnimazioneOggetto AnimazioneinPancia = target.getImmagine().getAnimazione(idAnimazioneinPancia);
        
        _nomeAnimazione.setText(AnimazioneinPancia.id);
        _Yanim.setText(String.valueOf(AnimazioneinPancia.yAnimazione));
        _Righe.setText(String.valueOf(AnimazioneinPancia.righeAnimazione));
        
        CBselezionata.setSelected(AnimazioneinPancia.eSelezionata);
    }
    
    //DATABIND SUL DROPDOWN IMPOSTO SELEZIONATO ID
    private void databindJcombo(String idDaSelezionare)
    {
        ComboAnimazioni.removeAllItems();
        
        for(int i=0; i< target.getImmagine().listaAnimazioni.size(); ++i)
        {
            ComboAnimazioni.addItem(target.getImmagine().listaAnimazioni.get(i).id);
        }
        
        //ALMENO UN ELEMENTO
        if(target.getImmagine().listaAnimazioni.size() > 0)
            ComboAnimazioni.setSelectedItem(idDaSelezionare);
    }
    //DATABIND SUL DROPDOWN IMPOSTO SELEZIONATO INDICE
    private void databindJcombo(int index)
    {
        ComboAnimazioni.removeAllItems();
        
        for(int i=0; i< target.getImmagine().listaAnimazioni.size(); ++i)
        {
            ComboAnimazioni.addItem(target.getImmagine().listaAnimazioni.get(i).id);
        }
        
        //ALMENO UN ELEMENTO
        if(target.getImmagine().listaAnimazioni.size() > 0)
            ComboAnimazioni.setSelectedIndex(index);
    }
}